-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2016 at 12:15 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ergasiabaseis`
--

-- --------------------------------------------------------

--
-- Table structure for table `borrower`
--

CREATE TABLE IF NOT EXISTS `borrower` (
  `Bid` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Town` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `StreetName` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `StreetNumber` int(4) unsigned NOT NULL,
  `PostalCode` int(4) NOT NULL,
  PRIMARY KEY (`Bid`),
  UNIQUE KEY `bid_unique` (`Bid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `borrower`
--

INSERT INTO `borrower` (`Bid`, `Name`, `Town`, `StreetName`, `StreetNumber`, `PostalCode`) VALUES
(44, 'Basilhs Georgiou', 'Athens', 'Karustou', 14, 15698),
(45, 'Antonis Trikouphs', 'Bolos', 'Omhrou', 44, 18978),
(46, 'Filippos Dimitriadhs', 'Athens', 'Akadhmias', 38, 16444),
(47, 'Stamatis Perikleous', 'Thessaloniki', 'Stadiou', 75, 12568),
(48, 'Swthrhs Petrou', 'Athens', 'Athinas', 23, 14665);

-- --------------------------------------------------------

--
-- Table structure for table `commitment`
--

CREATE TABLE IF NOT EXISTS `commitment` (
  `Lid` int(4) unsigned NOT NULL,
  `Bid` int(4) unsigned NOT NULL,
  `DateOfRequest` date NOT NULL,
  `Amount` int(4) unsigned NOT NULL,
  PRIMARY KEY (`Lid`,`Bid`,`DateOfRequest`),
  KEY `Bid` (`Bid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `commitment`
--

INSERT INTO `commitment` (`Lid`, `Bid`, `DateOfRequest`, `Amount`) VALUES
(15, 44, '2016-02-15', 11),
(16, 45, '2010-03-07', 39),
(17, 46, '2012-03-03', 10),
(18, 47, '2013-01-08', 16),
(19, 48, '2014-06-06', 18);

--
-- Triggers `commitment`
--
DROP TRIGGER IF EXISTS `sum_commitment`;
DELIMITER //
CREATE TRIGGER `sum_commitment` AFTER INSERT ON `commitment`
 FOR EACH ROW UPDATE sums
SET Value = Value + New.Amount
WHERE Origin=1
//
DELIMITER ;
DROP TRIGGER IF EXISTS `sum_commitment2`;
DELIMITER //
CREATE TRIGGER `sum_commitment2` AFTER DELETE ON `commitment`
 FOR EACH ROW UPDATE sums
SET Value = Value - Old.Amount
WHERE Origin=1
//
DELIMITER ;
DROP TRIGGER IF EXISTS `sum_commitment3`;
DELIMITER //
CREATE TRIGGER `sum_commitment3` AFTER UPDATE ON `commitment`
 FOR EACH ROW UPDATE sums
SET Value = Value + New.Amount - Old.Amount
WHERE Origin=1
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `dates`
--
CREATE TABLE IF NOT EXISTS `dates` (
`DateOfRequest` date
,`DateOfApproval` date
,`DateOfAgreement` date
,`DateOfPayment` date
);
-- --------------------------------------------------------

--
-- Table structure for table `deadline`
--

CREATE TABLE IF NOT EXISTS `deadline` (
  `Id` int(4) unsigned NOT NULL,
  `DateOfAgreement` date NOT NULL,
  `Deadline` date NOT NULL,
  PRIMARY KEY (`Id`,`DateOfAgreement`),
  KEY `Id` (`Id`),
  KEY `Id_2` (`Id`),
  KEY `DateOfAgreement` (`DateOfAgreement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deadline`
--

INSERT INTO `deadline` (`Id`, `DateOfAgreement`, `Deadline`) VALUES
(16, '2016-02-15', '2017-01-03'),
(17, '2010-03-07', '2012-02-02'),
(18, '2012-03-03', '2014-01-05'),
(19, '2013-01-08', '2015-01-01'),
(20, '2014-06-06', '2014-07-06');

-- --------------------------------------------------------

--
-- Table structure for table `intermediary`
--

CREATE TABLE IF NOT EXISTS `intermediary` (
  `Mid` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Town` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `StreetName` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `StreetNumber` int(4) unsigned NOT NULL,
  `PostalCode` int(4) unsigned NOT NULL,
  PRIMARY KEY (`Mid`),
  UNIQUE KEY `Mid` (`Mid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `intermediary`
--

INSERT INTO `intermediary` (`Mid`, `Name`, `Town`, `StreetName`, `StreetNumber`, `PostalCode`) VALUES
(9, 'Persefonh Stamatiou', 'Trikala', 'Vasilews', 1, 15222),
(10, 'Maria Ferekou', 'Athens', 'Thesalias', 27, 17874),
(11, 'Stelios Katsantwnhs', 'Thessaloniki', 'Kuprou', 72, 16552),
(12, 'Euthumhs Tranos', 'Bolos', 'Hpeirou', 41, 11223),
(13, 'Basilikh Androutsou', 'Athens', 'Persh', 36, 18554);

-- --------------------------------------------------------

--
-- Table structure for table `lender`
--

CREATE TABLE IF NOT EXISTS `lender` (
  `Lid` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Town` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `StreetName` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `StreetNumber` int(4) unsigned NOT NULL,
  `PostalCode` int(4) unsigned NOT NULL,
  PRIMARY KEY (`Lid`),
  UNIQUE KEY `Lid` (`Lid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `lender`
--

INSERT INTO `lender` (`Lid`, `Name`, `Town`, `StreetName`, `StreetNumber`, `PostalCode`) VALUES
(15, 'Antreas Filippou', 'Thessaloniki', 'Ampelokhpwn', 12, 15587),
(16, 'Konstantinos Papas', 'Karditsa', 'Imathias', 17, 19954),
(17, 'Basilhs Drakos', 'Athens', 'Durraxiou', 9, 16487),
(18, 'Gerasimos Leonths', 'Tripolh', 'Peristeriou', 51, 19888),
(19, 'Dhmhtrhs Koutsodhmos', 'Athens', 'Korinthou', 4, 15443);

-- --------------------------------------------------------

--
-- Table structure for table `loan`
--

CREATE TABLE IF NOT EXISTS `loan` (
  `Id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `DateOfApproval` date NOT NULL,
  `Mid` int(4) unsigned NOT NULL,
  `Bid` int(4) unsigned NOT NULL,
  `DateOfRequest` date NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `Bid` (`Bid`),
  KEY `Mid` (`Mid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `loan`
--

INSERT INTO `loan` (`Id`, `DateOfApproval`, `Mid`, `Bid`, `DateOfRequest`) VALUES
(16, '2016-03-01', 9, 44, '2016-02-15'),
(17, '2014-03-04', 10, 45, '2010-03-07'),
(18, '2014-01-07', 11, 46, '2012-03-03'),
(19, '2014-01-07', 12, 47, '2013-01-08'),
(20, '2014-07-07', 13, 48, '2014-06-06');

-- --------------------------------------------------------

--
-- Table structure for table `loanrequest`
--

CREATE TABLE IF NOT EXISTS `loanrequest` (
  `Bid` int(4) unsigned NOT NULL,
  `DateOfRequest` date NOT NULL,
  `Deadline` date NOT NULL,
  `Amount` int(4) unsigned NOT NULL,
  `PaybackPeriod` date NOT NULL,
  `Description` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Percentage` int(4) unsigned NOT NULL,
  PRIMARY KEY (`Bid`,`DateOfRequest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loanrequest`
--

INSERT INTO `loanrequest` (`Bid`, `DateOfRequest`, `Deadline`, `Amount`, `PaybackPeriod`, `Description`, `Percentage`) VALUES
(44, '2016-02-15', '2016-06-11', 29, '0000-04-04', 'Nothing Special', 25),
(45, '2010-03-07', '2012-03-07', 48, '0002-00-00', 'Something', 33),
(46, '2012-03-03', '2013-03-04', 55, '0001-00-01', 'Wrong', 48),
(47, '2013-01-08', '2013-02-08', 22, '0000-01-00', 'Right', 11),
(48, '2014-06-06', '2015-07-07', 77, '0001-01-01', 'Empty', 49);

-- --------------------------------------------------------

--
-- Table structure for table `repayment`
--

CREATE TABLE IF NOT EXISTS `repayment` (
  `Id` int(4) unsigned NOT NULL,
  `DateOfPayment` date NOT NULL,
  `Amount` int(4) unsigned NOT NULL,
  PRIMARY KEY (`Id`,`DateOfPayment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `repayment`
--

INSERT INTO `repayment` (`Id`, `DateOfPayment`, `Amount`) VALUES
(16, '2016-04-15', 15),
(17, '2011-03-07', 24),
(18, '2014-04-01', 14),
(19, '2013-01-09', 20),
(20, '2016-04-04', 18);

--
-- Triggers `repayment`
--
DROP TRIGGER IF EXISTS `sum_payments`;
DELIMITER //
CREATE TRIGGER `sum_payments` AFTER INSERT ON `repayment`
 FOR EACH ROW UPDATE sums
SET Value = Value + New.Amount
WHERE Origin=2
//
DELIMITER ;
DROP TRIGGER IF EXISTS `sum_payments2`;
DELIMITER //
CREATE TRIGGER `sum_payments2` AFTER DELETE ON `repayment`
 FOR EACH ROW UPDATE sums
SET Value = Value - Old.Amount
WHERE Origin=2
//
DELIMITER ;
DROP TRIGGER IF EXISTS `sum_payments3`;
DELIMITER //
CREATE TRIGGER `sum_payments3` AFTER UPDATE ON `repayment`
 FOR EACH ROW UPDATE sums
SET Value = Value + New.Amount - Old.Amount
WHERE Origin=2
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sums`
--

CREATE TABLE IF NOT EXISTS `sums` (
  `Origin` int(4) unsigned NOT NULL,
  `Value` int(4) unsigned NOT NULL,
  PRIMARY KEY (`Origin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sums`
--

INSERT INTO `sums` (`Origin`, `Value`) VALUES
(1, 94),
(2, 91);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sum_daneiwn`
--
CREATE TABLE IF NOT EXISTS `sum_daneiwn` (
`Name1` varchar(40)
,`Name2` decimal(32,0)
);
-- --------------------------------------------------------

--
-- Table structure for table `trust`
--

CREATE TABLE IF NOT EXISTS `trust` (
  `Bid` int(4) unsigned NOT NULL,
  `Lid` int(4) unsigned NOT NULL,
  `Percentage` float unsigned NOT NULL,
  PRIMARY KEY (`Bid`,`Lid`),
  KEY `Lid` (`Lid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trust`
--

INSERT INTO `trust` (`Bid`, `Lid`, `Percentage`) VALUES
(44, 15, 20),
(45, 16, 47),
(46, 17, 51),
(47, 18, 19),
(48, 19, 34);

-- --------------------------------------------------------

--
-- Structure for view `dates`
--
DROP TABLE IF EXISTS `dates`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dates` AS select `loan`.`DateOfRequest` AS `DateOfRequest`,`loan`.`DateOfApproval` AS `DateOfApproval`,`deadline`.`DateOfAgreement` AS `DateOfAgreement`,`repayment`.`DateOfPayment` AS `DateOfPayment` from ((`loan` join `deadline`) join `repayment`) where ((`loan`.`Id` = `repayment`.`Id`) and (`loan`.`Id` = `deadline`.`Id`)) order by `loan`.`DateOfRequest`;

-- --------------------------------------------------------

--
-- Structure for view `sum_daneiwn`
--
DROP TABLE IF EXISTS `sum_daneiwn`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sum_daneiwn` AS select `borrower`.`Name` AS `Name1`,sum(`commitment`.`Amount`) AS `Name2` from (`borrower` join `commitment`) where (`borrower`.`Bid` = `commitment`.`Bid`) group by `borrower`.`Bid`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `commitment`
--
ALTER TABLE `commitment`
  ADD CONSTRAINT `commitment_ibfk_2` FOREIGN KEY (`Bid`) REFERENCES `borrower` (`Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `commitment_ibfk_3` FOREIGN KEY (`Lid`) REFERENCES `lender` (`Lid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `deadline`
--
ALTER TABLE `deadline`
  ADD CONSTRAINT `deadline_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `loan` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `loan`
--
ALTER TABLE `loan`
  ADD CONSTRAINT `loan_ibfk_1` FOREIGN KEY (`Bid`) REFERENCES `borrower` (`Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `loan_ibfk_2` FOREIGN KEY (`Mid`) REFERENCES `intermediary` (`Mid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `loanrequest`
--
ALTER TABLE `loanrequest`
  ADD CONSTRAINT `loanrequest_ibfk_1` FOREIGN KEY (`Bid`) REFERENCES `borrower` (`Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `repayment`
--
ALTER TABLE `repayment`
  ADD CONSTRAINT `repayment_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `loan` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trust`
--
ALTER TABLE `trust`
  ADD CONSTRAINT `trust_ibfk_1` FOREIGN KEY (`Lid`) REFERENCES `lender` (`Lid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trust_ibfk_2` FOREIGN KEY (`Bid`) REFERENCES `borrower` (`Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
