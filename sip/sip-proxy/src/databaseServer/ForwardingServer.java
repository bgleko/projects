package databaseServer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ForwardingServer {
	
	public static SqlServer sqlserver;
	
	public ForwardingServer(SqlServer sql)
	{
		sqlserver=sql;
	}
	
	public boolean insert(String name,String nameTo){
		try{
		/*	Check if both users exist	*/
		boolean flag=true;
		String sqlQuery=
				"SELECT * FROM users WHERE username = '"+name+"'";
		ResultSet rs=sqlserver.result(sqlQuery);
		flag=flag && rs.next();
		sqlQuery=
				"SELECT * FROM users WHERE username = '"+nameTo+"'";
		rs=sqlserver.result(sqlQuery);
		flag=flag && rs.next();
		if (!flag)
			return false;
		
		/*	Check if nameTo exists and Insert or Update		*/
		boolean nameHasForw;
		sqlQuery=
				"SELECT * FROM forward WHERE forwarder = '"+name+"'";
		rs=sqlserver.result(sqlQuery);
		nameHasForw=rs.next();
		if(nameHasForw)
			sqlQuery=
			"UPDATE forward SET forwardee = '"+nameTo+"' WHERE forwarder = '"+name+"'";
		else
			sqlQuery=
			"INSERT INTO forward (forwarder, forwardee) VALUES('"+name+"','"+nameTo+"')";
		int result=sqlserver.update(sqlQuery);
		if (result!=0)	return true;
		else return false;
		}
	catch(Exception e){
		e.printStackTrace();
		return false;
	}
	}
	
	public boolean remove(String name){
		try{
		boolean flag=true;
		String sqlQuery=
				"SELECT username FROM users WHERE username = '"+name+"'";
		ResultSet rs=sqlserver.result(sqlQuery);
		flag=flag && rs.next();
		if (!flag)
			return false;
		
		/*	Check if nameTo exists and Insert or Upda		*/
		boolean nameHasForw;
		sqlQuery=
				"SELECT * FROM forward WHERE forwarder = '"+name+"'";
		rs=sqlserver.result(sqlQuery);
		nameHasForw=rs.next();
		if(nameHasForw){
			sqlQuery=
					"DELETE FROM forward WHERE forwarder = '"+name+"'";
			int result=sqlserver.update(sqlQuery);
		}
		else
			return false;
		
		return true; 
		}
	catch(Exception e){
		e.printStackTrace();
		return false;
	}
	}

	public boolean isForwarded(String name) {
		String sqlQuery=
				"SELECT * FROM forward WHERE forwarder = '"+name+"'";
		ResultSet rs=sqlserver.result(sqlQuery);
		boolean isforwarded=false;
		try {
			isforwarded= rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return isforwarded;
	}

	public String getForwardee(String name) {
		String sqlQuery=
				"SELECT * FROM forward WHERE forwarder = '"+name+"'";
		ResultSet rs=sqlserver.result(sqlQuery);
		String forwardee="";
		try {
			rs.next();
			forwardee=rs.getString("forwardee");
			return forwardee;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return forwardee;
	}

	public String getFinalForwardee(String caller,String forwarder,boolean print) {
		ArrayList<String> forwardingList=new ArrayList<String>();
		forwardingList.add(caller);
		forwardingList.add(forwarder);
		System.out.println("Caller:"+caller+" to:"+forwarder);
		String tempForwarder=forwarder;
		do{
			System.out.println("Array is size:"+forwardingList.size());
			tempForwarder=getForwardee(tempForwarder);
			if(forwardingList.contains(tempForwarder)){
				/*	Circle found	*/
				 if(print) System.out.println();
				 if(print) System.out.println("## CIRCLE DETECTED (bonus) ##");
	       		 forwardingList.add(tempForwarder);
	       		 int counter=0;
	       		 int circleCounter=0;
	       		 for (String name: forwardingList){
	       			circleCounter++;
	       			 
	       			 if(name.equals(tempForwarder)){
	       				 if(print)
	       				 System.out.print("$");
	       				 counter++;
	       				 if (counter==2){
	       					 if(print) System.out.print(name);
	       					 break;
	       				 }
	       			 }
	       			 if(print)System.out.print(name);
	       			 if(print)System.out.print("->");
	       		 }
	       		System.out.println();
	       		System.out.println("size= "+circleCounter);
	       		System.out.println();
				return null;
			}
			forwardingList.add(tempForwarder);
			System.out.println("And then to:"+tempForwarder);
		}while(isForwarded(tempForwarder));
		
		return forwardingList.get(forwardingList.size()-1);
	}
	
	
}
