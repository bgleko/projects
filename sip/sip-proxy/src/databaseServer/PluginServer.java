package databaseServer;

import gov.nist.sip.proxy.Configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import databaseServer.ForwardingServer;
import databaseServer.BillingServer;

public class PluginServer extends Thread{
	public static ForwardingServer forwardingServer;
	public static BillingServer billingServer;
	public static BlockingServer blockingServer;
	public static RegistrationServer registrationServer;
	public static Configuration configuration;
	public static SqlServer sqlserver;
	
	public PluginServer(Configuration config)
	{
		configuration=config;
		sqlserver= new SqlServer();
		forwardingServer= new ForwardingServer(sqlserver);
		billingServer= new BillingServer(sqlserver);
		blockingServer= new BlockingServer(sqlserver);
		registrationServer= new RegistrationServer(sqlserver);
	}
	
	public void run(){
	   System.out.println("MyThread running");
	   ServerSocket SrvSocket=null;
	   try {
	   System.out.println("Connecting to "+configuration.stackIPAddress+" to port 4001");
	   SrvSocket=new ServerSocket(4001,5,InetAddress.getByName(configuration.stackIPAddress));
	   System.out.println("Entered PLUGIN SERVER $############################$");
	   while(true){
		  
			   Socket socket=SrvSocket.accept();
			   InputStreamReader inputStreamReader=new InputStreamReader(socket.getInputStream());
			   BufferedReader bufferedReader =new BufferedReader(inputStreamReader);
			   String message=bufferedReader.readLine();
			   System.out.println("[Plugin Server]:Got this: "+message);
			   
			   String reply=dosth(message);
			   
			   PrintStream PS=new PrintStream(socket.getOutputStream());
			   PS.println(reply);
			   
	   }
	   		
	   } catch (IOException e) {
			e.printStackTrace();
			System.out.println("Socket 4001 Closed. Plugins exiting...");
			
			return;
		}
	   finally{
		   try {
			SrvSocket.close();
		} catch (IOException e) {
			System.out.println("Shouldnt close but closed");
			e.printStackTrace();
		}
	   }
	   
	}
	   
	private String dosth(String message){
		String [] split=message.split("-");
		if(split[0].equals(PluginProtocol.loginCode)){
			String name=split[1];
			String password=split[2];
			System.out.println("Registration Server:Search "+name+" password "+password);
			boolean exists=registrationServer.exists(name,password);
			if(exists) System.out.println("RegistrationServer:Connection succeeded");
			else System.out.println("RegistrationServer:Connection failed");
			if (exists)
				return "Success";
			else
				return "Failed";
		}
		
		else if(split[0].equals(PluginProtocol.forwardCode)){
			String name=split[1];
			String nameTo=split[2];
			System.out.println("[Plugin Server]:Want to forward:"+name+" to:"+nameTo);
			boolean done=forwardingServer.insert(name, nameTo);
			if (done)
				return "Success";
			else
				return "Failed";
		}
		else if(split[0].equals(PluginProtocol.unforwardCode)){
			String name=split[1];
			boolean done=forwardingServer.remove(name);
			if (done)
				return "Success";
			else
				return "Failed";
		}
		else if(split[0].equals(PluginProtocol.blockingCode)){
			String name=split[1];
			String nameTo=split[2];
			boolean done=blockingServer.insert(name, nameTo);
			if (done)
				return "Success";
			else
				return "Failed";
		}
		else if(split[0].equals(PluginProtocol.unblockingCode)){
			String name=split[1];
			String nameTo=split[2];
			boolean done=blockingServer.remove(name, nameTo);
			if (done)
				return "Success";
			else
				return "Failed";
		}
		else if(split[0].equals(PluginProtocol.billingCode)){
			String name=split[1];
			double cost=Double.parseDouble(split[2]);
			boolean done=billingServer.insert(name,cost);
			if(done) return "Success";
			else return "Failed";
		}
		else if(split[0].equals(PluginProtocol.billingPolicyCode)){
			String name=split[1];
			int policy=Integer.valueOf(split[2]);
			if(policy>2 || policy<0) return "Failed";
			int result=billingServer.setPolicy(name,policy);
			if(result==0) return "Success";
			else return "Failed";
		}
		else if(split[0].equals(PluginProtocol.registerCode)){
			String userName=split[1];
			String password=split[2];
			String email=split[3];
			String bankAccount=split[4];
			boolean done=registrationServer.insert(userName, password, email, bankAccount);
			billingServer.setPolicy(userName,0);
			if (done)
				return "Success";
			else
				return "Failed";
		}
		else if(split[0].equals(PluginProtocol.isforward)){
			String myName=split[1];
			String name=split[2];
			boolean is=forwardingServer.isForwarded(name);
			if (!is)
				return "no it isn`t";
			 String finalTarget = forwardingServer.getFinalForwardee(myName,name,false);
			 if (finalTarget==null)
				 return "no it isn`t";
	//		 System.out.println("isforwarder returns:"+finalTarget);
			return finalTarget;
		}
		
		return "";
	}
}
