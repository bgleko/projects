package databaseServer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class BillingServer {
	
public static SqlServer sqlserver;
	
	ArrayList timer = null;
	ArrayList caller = null;
	public BillingServer(SqlServer sql)
	{
		timer=new ArrayList<Long>();
		caller=new ArrayList<String>();
		sqlserver=sql;
	}
	
	public void chargeCaller(String caller, String callee,double cost)
	{
		int i;
		if(this.caller.contains(caller))
		{
			i=this.caller.indexOf(caller);
		}
		else if(this.caller.contains(callee))
		{
			i=this.caller.indexOf(callee);
		}
		else return;
		insert((String)this.caller.get(i),cost);
		this.caller.remove(i);
		this.timer.remove(i);
	}
	
	public boolean insert(String name,double cost){
		try{
		boolean flag=true;
		String sqlQuery=
				"SELECT * FROM users WHERE username = '"+name+"'";
		ResultSet rs=sqlserver.result(sqlQuery);
		flag=flag && rs.next();
		if (!flag)
			return false;
		cost=cost+rs.getDouble("cost");
		sqlQuery=
			"UPDATE users SET cost = "+cost+" WHERE username = '"+name+"'";
		int result=sqlserver.update(sqlQuery);
		if(result==0) return false;
		return true; 
		}
	catch(Exception e){
		e.printStackTrace();
		return false;
	}
	}
	
	public int getPolicy(String name){
		try{
			boolean flag=true;
			String sqlQuery=
					"SELECT policy FROM policies WHERE username = '"+name+"'";
			ResultSet rs=sqlserver.result(sqlQuery);
			flag=flag && rs.next();
			if (!flag)
				return -1;
			int policy=rs.getInt("policy");
			return policy; 
			}
		catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}
	
	public int setPolicy(String name,int policy){
		try{
			boolean flag=true;
			String sqlQuery=
					"SELECT policy FROM policies WHERE username = '"+name+"'";
			ResultSet rs=sqlserver.result(sqlQuery);
			flag=flag && rs.next();
			if (!flag) sqlQuery="INSERT INTO policies (username,policy) VALUES('"+name+"',"+Integer.valueOf(policy).toString()+")";
			else sqlQuery="UPDATE policies SET policy="+Integer.valueOf(policy).toString()+" WHERE username='"+name+"'";
			int result=sqlserver.update(sqlQuery);
			if(result!=0) return 0;
		}
		catch(Exception e){
			e.printStackTrace();
			return -1;
		}
		return -1;
	}

	public double calculateCost(String caller, String callee)
	{
		int i;
		if(this.caller.contains(caller))
		{
			i=this.caller.indexOf(caller);
		}
		else if(this.caller.contains(callee))
		{
			i=this.caller.indexOf(callee);
		}
		else return 0.0;
		long timer=(Long) this.timer.get(i);
		double value=0.0;
		int policy=getPolicy((String)this.caller.get(i));
		if(policy==0) value=(0.03*timer);
		else if(policy==1) value=(3.0+(timer*1.0-3.0)*0.02);
		else if(policy==2) value=(10.0);
		return value;
	}
	
	public void callStart(String caller, String callee) {
		java.util.Date javaDate = new java.util.Date();
        long javaTimeStart = javaDate.getTime();
        int i;
        if(this.caller.contains(caller)) {
        	i=this.caller.indexOf(caller);
        	this.timer.set(i,javaTimeStart);
        }
        else
        {
        	this.caller.add(caller);
        	this.timer.add(javaTimeStart);
        }
		
	}

	public void callEnd(String caller, String callee) {
		java.util.Date javaDate = new java.util.Date();
        long javaTimeEnd = javaDate.getTime();
        int i;
        if(this.caller.contains(caller))
        {
        	i=this.caller.indexOf(caller);
        	long javaTimeStart=(Long) this.timer.get(i);
        	this.timer.set(i,(javaTimeEnd-javaTimeStart));
        }
        else if(this.caller.contains(callee))
        {
        	i=this.caller.indexOf(callee);
        	long javaTimeStart=(Long) this.timer.get(i);
        	this.timer.set(i,(javaTimeEnd-javaTimeStart));
        }
	}
}
