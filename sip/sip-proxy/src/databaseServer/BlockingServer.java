package databaseServer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BlockingServer {
	
public static SqlServer sqlserver;
	
	public BlockingServer(SqlServer sql)
	{
		sqlserver=sql;
	}
	
	public boolean insert(String name,String nameTo){
		try{
			boolean flag=true;
			String sqlQuery=
					"SELECT username FROM users WHERE username = '"+name+"'";
			ResultSet rs=sqlserver.result(sqlQuery);
			flag=flag && rs.next();
			sqlQuery=
					"SELECT * FROM users WHERE username = '"+nameTo+"'";
			rs=sqlserver.result(sqlQuery);
			flag=flag && rs.next();
			if (!flag)
				return false;
			
			sqlQuery=
			"INSERT INTO block (blocker, blocked) VALUES('"+name+"','"+nameTo+"')";
			int result=sqlserver.update(sqlQuery);
			if(result!=0) return true;
			else return false;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean remove(String name,String nameTo){
		try{
			boolean flag=true;
			String sqlQuery=
					"SELECT username FROM users WHERE username = '"+name+"'";
			ResultSet rs=sqlserver.result(sqlQuery);
			flag=flag && rs.next();
			sqlQuery=
					"SELECT * FROM users WHERE username = '"+nameTo+"'";
			rs=sqlserver.result(sqlQuery);
			flag=flag && rs.next();
			if (!flag)
				return false;
			boolean nameHasBlock;
			sqlQuery=
					"SELECT * FROM block WHERE blocker = '"+name+"' AND blocked = '"+nameTo+"'";
			rs=sqlserver.result(sqlQuery);
			nameHasBlock=rs.next();
			if(nameHasBlock){
				sqlQuery=
				"DELETE FROM block WHERE blocker = '"+name+"'AND blocked ='"+nameTo+"'";
				int result=sqlserver.update(sqlQuery);
			}
			else
				return false;
			
			return true; 
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public boolean isBlocked(String nameFrom, String nameTo) {
		String sqlQuery=
				"SELECT * FROM block WHERE blocker = '"+nameTo+"' AND blocked = '"+nameFrom+"'";
		ResultSet rs=sqlserver.result(sqlQuery);
		boolean isblocked= false;
		try {
			isblocked=rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return isblocked;
	}
}
