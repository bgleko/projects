package databaseServer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlServer {
	private static Connection conn = null;
	
	public SqlServer(){
	try{
		Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection
		("jdbc:mysql://localhost:3306/j-comm-db?user=root&password=42");
	}
	catch(ClassNotFoundException cnfe){
		System.out.println("Shouldnt close but closed");
		cnfe.printStackTrace();
	}
	catch(SQLException sqle){
		System.out.println("Shouldnt close but closed");
		sqle.printStackTrace();
	}

	}
	
	public static ResultSet result(String sqlQuery) {
		 PreparedStatement stmt=null;
		 ResultSet rs=null;
		try {
			stmt = conn.prepareStatement(sqlQuery);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
        try {
			rs= stmt.executeQuery();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return rs;
		}
	}
	
	public static int update(String sqlQuery) {
		 PreparedStatement stmt=null;
		 int rs=0;
		try {
			stmt = conn.prepareStatement(sqlQuery);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
       try {
			rs= stmt.executeUpdate();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return rs;
		}
	}
}
