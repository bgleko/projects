package net.java.sip.communicator.gui.config;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import net.java.sip.communicator.sip.security.UserCredentials;

public class RegistrationPanel extends JDialog
{
	private String userName = null;
	//private String password = null;
	private char[] password = null;
	private String email = null;
	private String bankAccount = null;
	
	private Container container;
	private JPanel mainPanel;
	private JLabel messageLabel;
	private JLabel userNameLabel;
	private JLabel passwordLabel;
	private JLabel emailLabel;
	private JLabel bankAccountLabel;	

	private JTextField userNameText = null;
	private JPasswordField passwordText = null;
	private JTextField emailText = null;
	private JTextField bankAccountText = null;

	private JPanel userNamePanel;
	private JPanel passwordPanel;
	private JPanel emailPanel;
	private JPanel bankAccountPanel;
		
	public RegistrationPanel(boolean modal/*,boolean isConnected*/)
	{
		super((Frame) null,modal);
		
					
		//	init panel
		try
		{
			jbInit();
			pack();
			setResizable(false);
			setLocationRelativeTo(null);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void jbInit()
	{
		//basic container
	    container = getContentPane();
	    container.setLayout(new BorderLayout());
	    mainPanel = new JPanel();
	    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		//title
	    String title="Registration";
	    setTitle(title);
		
		//message label
	    messageLabel = new JLabel("Sign Up");
		
		//text field label for USERNAME
	    userNamePanel = new JPanel();
	    userNamePanel.setLayout(new BoxLayout(userNamePanel, BoxLayout.X_AXIS));
	    userNameText=new JTextField();
	    userNameText.setPreferredSize( new Dimension( 200, 24 ) );
	    userNameLabel = new JLabel();
	    userNameLabel.setLabelFor(userNameText);
	    userNameLabel.setText("User Name: ");
		
		//text field label for PASSWORD
	    passwordPanel = new JPanel();
	    passwordPanel.setLayout(new BoxLayout(passwordPanel, BoxLayout.X_AXIS));
	    passwordText= new JPasswordField(); //needed below
	    //passwordText=new JPasswordField();
	    passwordText.setPreferredSize( new Dimension( 200, 24 ) );
	    passwordLabel = new JLabel();
	    passwordLabel.setLabelFor(passwordText);
	    passwordLabel.setText("Password: ");
		
		//text field label  for EMAIL
	    emailPanel = new JPanel();
	    emailPanel.setLayout(new BoxLayout(emailPanel, BoxLayout.X_AXIS));
	    emailText=new JTextField();
	    emailText.setPreferredSize( new Dimension( 200, 24 ) );
	    emailLabel = new JLabel();
	    emailLabel.setLabelFor(emailText);
	    emailLabel.setText("E-mail Address: ");
		
		//text field label for BANKACCOUNT
	    bankAccountPanel = new JPanel();
	    bankAccountPanel.setLayout(new BoxLayout(bankAccountPanel, BoxLayout.X_AXIS));
	    bankAccountText=new JTextField();
	    bankAccountText.setPreferredSize( new Dimension( 200, 24 ) );
	    bankAccountLabel = new JLabel();
	    bankAccountLabel.setLabelFor(bankAccountText);
	    bankAccountLabel.setText("Bank Account: ");
		
		// panel with 2 buttons
	    JPanel buttonPanel = new JPanel();
		
		// Register Button
	    JButton RegisterButton = new JButton();
		RegisterButton.setText("Register");
	    RegisterButton.addActionListener(new ActionListener(){
	        public void actionPerformed(ActionEvent event){
	        	userName = userNameText.getText();
	        	password = passwordText.getPassword();
	        	email = emailText.getText();
	        	bankAccount = bankAccountText.getText();	  
	        	String message=PluginProtocol.registerCode +"-"+ userName +"-"+(new String(password))+"-"+email+"-"+bankAccount;
	        	String result="Failed";
	        	try {
					result = PluginProtocol.send(message);
				} catch (Exception e) {
					e.printStackTrace();
				}
	        	
	        	if (result.equals("Success"))
	        		JOptionPane.showMessageDialog(
							null, "Registration done.", "Success", JOptionPane.INFORMATION_MESSAGE);
	        	else{
	        		JOptionPane.showMessageDialog(
							null, "Registration not done.", "Failed", JOptionPane.INFORMATION_MESSAGE);
	        		return;
	        	}
	        	setVisible(false);
	            dispose();
	        }
	    });
		
		//cancel button
	    JButton cancelButton = new JButton();
	    cancelButton.setText("Cancel");
	    cancelButton.addActionListener(new ActionListener(){
	        public void actionPerformed(ActionEvent event){
	        	setVisible(false);
	        	dispose();
	        }
	    });
		
		//adds
	    buttonPanel.add(RegisterButton,BorderLayout.WEST);
	    buttonPanel.add(cancelButton,BorderLayout.EAST);
	    
		userNamePanel.add(userNameLabel);
		userNameLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		userNamePanel.add(userNameText);
		userNamePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		passwordPanel.add(passwordLabel);
		passwordLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		passwordPanel.add(passwordText);
		passwordPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		emailPanel.add(emailLabel);
		emailLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		emailPanel.add(emailText);
		emailPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		bankAccountPanel.add(bankAccountLabel);
		bankAccountLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		bankAccountPanel.add(bankAccountText);
		bankAccountPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		
	    mainPanel.add(messageLabel);
	    mainPanel.add(userNamePanel);
	    mainPanel.add(passwordPanel);
	    mainPanel.add(emailPanel);
	    mainPanel.add(bankAccountPanel);
		
	    messageLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
	    userNamePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
	    passwordPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
	    emailPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
	    bankAccountPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
	    
	    container.add(mainPanel);
	    container.add(buttonPanel,BorderLayout.SOUTH);
	
	}
	
}