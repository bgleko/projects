package net.java.sip.communicator.gui.config;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import net.java.sip.communicator.sip.security.UserCredentials;

public class ForwardingPanel
extends JDialog
{
	private String myName = null;
	private String targetName = null;
	private JTextField targetNameText = null;
	
	private Container container;
	private JPanel fieldPanel;
	private JLabel messageLabel;
	private JLabel  targetUserNameLabel;
	public ForwardingPanel(boolean modal,UserCredentials credentials)
	{
		super((Frame) null,modal);
		
		//	fill this.myName from credentials
		String name="";
		if (credentials!=null)
			name=credentials.getUserName();
		if (name==null)
			name="";
		this.myName=name;
			
		//	init panel
		try
		{
			jbInit();
			pack();
			setResizable(false);
			setLocationRelativeTo(null);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void jbInit()
	{
		//basic container
	    container = getContentPane();
	    container.setLayout(new BorderLayout());
	    
	    //title
	    String title="Forwarding";
	    setTitle(title);
	    
	    //message label
	    messageLabel = new JLabel("Please enter target username to (un)forward");
	    
	    //text field label
	    fieldPanel = new JPanel();
	    targetNameText=new JTextField();
	    targetNameText.setPreferredSize( new Dimension( 200, 24 ) );
	    targetUserNameLabel = new JLabel();
	    targetUserNameLabel.setLabelFor(targetNameText);
	    targetUserNameLabel.setText("User Name:");
	    
	    // panel with 3 buttons
	    JPanel buttonPanel = new JPanel();
	    
	    	// Forward Button
	    JButton forwardToButton = new JButton();
	    forwardToButton.setText("Forward");
	    forwardToButton.addActionListener(new ActionListener(){
	        public void actionPerformed(ActionEvent event){
	        	targetName = targetNameText.getText();
	        	String message=PluginProtocol.forwardCode+"-"+ myName +"-"+ targetName;
	        	String result="Failed";
	        	try {
					result = PluginProtocol.send(message);
				} catch (Exception e) {
					e.printStackTrace();
				}
	        	
	        	if (result.equals("Success"))
	        		JOptionPane.showMessageDialog(
							null, "Forwarding done.", "Success", JOptionPane.INFORMATION_MESSAGE);
	        	else{
	        		JOptionPane.showMessageDialog(
							null, "Forwarding not done.", "Failed", JOptionPane.INFORMATION_MESSAGE);
	        		return;
	        	}
	        	setVisible(false);
	            dispose();
	        }
	    });
	    
	        // unForward Button
	    JButton unforwardToButton = new JButton();
	    unforwardToButton.setText("Unforward");	    
	    unforwardToButton.addActionListener(new ActionListener(){
	        public void actionPerformed(ActionEvent event){
	        	targetName = targetNameText.getText();
	        	String message=PluginProtocol.unforwardCode+"-"+ myName;
	        	String result="Failed";
	        	try {
					result = PluginProtocol.send(message);
				} catch (Exception e) {
					e.printStackTrace();
				}
	        	
	        	if (result.equals("Success"))
	        		JOptionPane.showMessageDialog(
							null, "Unforwarding done.", "Success", JOptionPane.INFORMATION_MESSAGE);
	        	else{
	        		JOptionPane.showMessageDialog(
							null, "Unforwarding not done.", "Failed", JOptionPane.INFORMATION_MESSAGE);
	        		return;
	        	}
	        	setVisible(false);
	            dispose();
	        }
	    });
	    
	    	//cancel button
	    JButton cancelButton = new JButton();
	    cancelButton.setText("Cancel");
	    cancelButton.addActionListener(new ActionListener(){
	        public void actionPerformed(ActionEvent event){
	        	setVisible(false);
	        	dispose();
	        }
	    });
	    
	    //	adds
	    buttonPanel.add(forwardToButton,BorderLayout.WEST);
	    buttonPanel.add(unforwardToButton,BorderLayout.CENTER);
	    buttonPanel.add(cancelButton,BorderLayout.EAST);
	    
	    fieldPanel.add(targetUserNameLabel,BorderLayout.WEST);
	    fieldPanel.add(targetNameText,BorderLayout.EAST);
	    container.add(buttonPanel,BorderLayout.SOUTH);
	    container.add(messageLabel, BorderLayout.NORTH);
	    container.add(fieldPanel);	//, BorderLayout.CENTER);
	}

	private String senddummy(String message){
		return "Success";
	}
}
