package net.java.sip.communicator.gui.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public final class PluginProtocol {
	
	public static final String PluginProxyIp="192.168.1.5";
	public static final int PluginProxyPort=4001;
	
	public static final String loginCode="0";
	public static final String registerCode="1";
	public static final String unregisterCode="2";
	public static final String billingCode="3";
	public static final String billingPolicyCode="4";
	public static final String blockingCode="5";
	public static final String unblockingCode="6";
	public static final String forwardCode="7";
	public static final String unforwardCode="8";
	public static final String isforward="9";
	
	public static String send(String message) throws UnknownHostException, IOException {
		System.out.println("[PluginProtocol]: Sending message: "+message);
		Socket socket=new Socket(InetAddress.getByName(PluginProxyIp),PluginProxyPort);
		PrintStream printstream=new PrintStream(socket.getOutputStream());
		printstream.println(message);
		
		InputStreamReader inputstream=new InputStreamReader(socket.getInputStream());
		BufferedReader buffreader=new BufferedReader(inputstream);
		String reply=buffreader.readLine();
		System.out.println("PLUGIN PROTOCOL: THE RESULT OF TRYING TO LOGIN IS: "+reply);
		socket.close();
		return reply;
	}
}
