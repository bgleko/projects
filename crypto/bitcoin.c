#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>
//#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>


#include <openssl/opensslconf.h>
#include <openssl/crypto.h>
#include <openssl/sha.h>
#include <openssl/opensslv.h>


#ifndef _SOCKET_COMMON_H
#define _SOCKET_COMMON_H

/* Compile-time options */
#define TCP_PORT(i)    25000+i
#define TCP_BACKLOG 5

#define HELLO_THERE "Hello there!"

#endif /* _SOCKET_COMMON_H */

#define MAX_TRANSACTIONS 3
#define MAX_TARGET (1<<20)
#define FIRST_HASH "1234567890"
#define CURRENT_REWARD 25

//*****STRUCTS************************************************************************************//
struct sa_list
{
	struct sockaddr_in sa;
	struct sa_list *next;
};

struct intlist
{
	int value;
	struct intlist *next;
};

struct transaction
{
	int id_from; //if it's a reward id_from=0
	int id_to;
	int timestamp;
	unsigned int extranonce;//if it's not a reward its value is irrelevant
	int pubkey; //if it's a reward the public key refers to the miner
	int ammount;
};

struct transaction_list
{
	struct transaction trans;
	struct transaction_list *next;
};

struct header
{
	int version;
	unsigned char hashPrevBlock[SHA256_DIGEST_LENGTH];	//prev block hash
	unsigned char hashMerkleRoot[SHA256_DIGEST_LENGTH]; //block hash
	int Time;
	unsigned int Bits;	//hash limit
	unsigned int Nonce;
};

struct block
{
	int magicNo;
	int Blocksize;//the blocks the blockchain consists of
	struct header blockheader;
	int transactionsCounter;
	struct transaction_list *transactions; //first transaction is always the reward
	struct block *next;
	int checked;
	double chaindifficulty;
};

struct blockedges
{
	struct block *blockAddress;
	struct blockedges *next;
};

struct info //first packet for every sending in order to notify for the type and the size of the data to send
{
	int type;	//type 1 means block chain, type 2 pending transactions, type 3 new block, type 4 new transaction
	size_t size;
};

struct pidlist
{
    int pid;
    struct pidlist* next;
};
//*************************************************************************************************//


//********GLOBAL DATA******************************************************************************//
struct block *lastblock;
struct blockedges *local_blocklist;
struct transaction_list *pending_transactions, *newesttransaction;
int id;
int centralpid;
int wallet;
int privkey;
int pubkey;
char buffer[1000];
struct intlist *outsd,*insd;
int number_pids,number_inpids;
int current_target;
int maxsd;
//*************************************************************************************************//


ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;
	
	while (cnt > 0) {//continue writing the data given in the fd until the appropriate ammount of data is indeed writen in the fd
	        ret = write(fd, buf, cnt);
	        if (ret < 0)
	                return ret;
	        buf += ret;
	        cnt -= ret;
	}

	return orig_cnt;
}

void disconnectUser(int client,struct intlist *currentsd)
{
	if(client)
	{
		if (shutdown(currentsd->next->value, SHUT_WR) < 0) {
			perror("shutdown");
			exit(1);
		}	
	}
	struct intlist *templist;
	templist=currentsd->next;
	currentsd->next=currentsd->next->next;
	free(templist);	
}


void PidToString(char *result2, int pid)
{
    int i=0;
	char result[10];
    while(pid!=0)
    {
        result[i]=pid%10+'0';
        pid=pid/10;
        i++;
    }
    result[i]='\0';
    i--;
    int j=0;//convert the pid value to string format
    char tempchar;
    while(j<i)
    {
        tempchar=result[j];
        result[j]=result[i];
        result[i]=tempchar;
        j++; i--;
    }
	strcpy(result2,result);
}

void addpid(char *pids,int pid)
{
    int length=strlen(pids);
	if(strlen(pids)!=0) strcat(pids," ");
	char result[10];	//add the given pid at the end of the string
	PidToString(result,pid); 
	strcat(pids,result);
}

void findpids(struct pidlist *envpid, char *pids)
{
    int i=0;
    int end=strlen(pids);
    int curpid=0;
    struct pidlist *templist,*current;
	for(i=0;i<end+1;i++)
    {
        if(pids[i]==' ' || pids[i]=='\n' || pids[i]=='\t' || pids[i]=='\0')
        {
            if(curpid!=0)//make a node for each pid found in the read list
            {
                current=(struct pidlist*)malloc(sizeof(struct pidlist));
                current->pid=curpid;
                curpid=0;
                templist=envpid;
                while(templist->next!=NULL)
                {
                    templist=templist->next;
                }
                templist->next=current;
                current->next=NULL;
            }
        }
        else
        {
            curpid=curpid*10+pids[i]-'0';
        }
    }
}

void pidlistToString(char *pids, struct pidlist *envpid)
{
    pids[0]='\0';
    struct pidlist *templist;
    templist=envpid;
    char result2[10];
    while(templist->next!=NULL)
    {
        PidToString(result2,templist->next->pid);
        strcat(pids,result2);//convert pid list in format writable to the file
        templist=templist->next;        
        if(templist->next!=NULL) strcat(pids," ");
    }
}


char *perform_hashing(struct transaction_list *transactions,int counter, unsigned char *finalhash)
{
	int flag=0;	
	if(counter==0)
	{
		memset(finalhash,0,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
		strcpy(finalhash,FIRST_HASH);
		return finalhash;
	}
	if(counter%2==1 && counter!=1)
	{
		counter+=1;	
		flag=1;
	}
	int orig=counter;	
	unsigned char **hashes;
	hashes=(unsigned char**)malloc(counter*sizeof(unsigned char*));
	hashes[0]=(unsigned char*)malloc(SHA256_DIGEST_LENGTH*counter*sizeof(unsigned char));
	int i;	
	for (i=1;i<counter;i++)
	{
		hashes[i]=hashes[0]+i*SHA256_DIGEST_LENGTH;//the matrices to store the hash inputs and results ATTENTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	}
	unsigned char **input;
	input=(unsigned char**)malloc(counter*sizeof(unsigned char*));
	for (i=0;i<counter;i++)
	{
		input[i]=(unsigned char*)malloc(SHA256_DIGEST_LENGTH*sizeof(unsigned char));
		memset(input[i],0,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	}	
	struct transaction_list *temptrans=transactions;	
	for(i=0;i<counter-1;i++)
	{	
		memcpy(input[i],&temptrans->trans,sizeof(struct transaction));//copy inputs in appropriate format
		temptrans=temptrans->next;	
	}	
	if(flag)memcpy(input[counter-1],input[counter-2],SHA256_DIGEST_LENGTH*sizeof(unsigned char));	
	else
	{
		memcpy(input[counter-1],&temptrans->trans,sizeof(struct transaction));
		temptrans=temptrans->next;
	}
	for(i=0;i<counter;i++)
	{
		memset(hashes[i],0,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
		hashes[i]=SHA256(input[i],SHA256_DIGEST_LENGTH*sizeof(unsigned char),hashes[i]);//find hash of each transaction
	}	
	counter/=2;
	while(counter!=0)
	{
		flag=0;
		if(counter%2==1 && counter!=1)
		{
			counter++;
			flag=1;
		}
		if(flag)
		{
			memcpy(hashes[counter-1],hashes[counter-2],SHA256_DIGEST_LENGTH*sizeof(unsigned char));	
		}
		for(i=0;i<counter;i++)
		{
			hashes[i]=SHA256(hashes[2*i],2*SHA256_DIGEST_LENGTH*sizeof(unsigned char),hashes[i]);//find hash of pairs of hashes creating a tree of hashes
		}
		counter/=2;
	}
	memcpy(finalhash,hashes[0],SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	for(i=0;i<orig;i++) free(input[i]);
	free(hashes[0]);
	free(hashes);
	free(input);	
	return finalhash;//return the root hash of the tree
}


int verify_block(struct block *currentblock)
{
	unsigned int nonce=currentblock->blockheader.Nonce;
	unsigned int extranonce=currentblock->transactions->trans.extranonce;
	unsigned char Bit[SHA256_DIGEST_LENGTH];
	unsigned char input[2*SHA256_DIGEST_LENGTH+2*sizeof(unsigned int)/sizeof(unsigned char)];
	memcpy(input,currentblock->blockheader.hashMerkleRoot,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	memcpy((input+SHA256_DIGEST_LENGTH),currentblock->blockheader.hashPrevBlock,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	memcpy((input+2*SHA256_DIGEST_LENGTH),&currentblock->blockheader.Nonce,sizeof(unsigned int));
	memcpy((input+2*SHA256_DIGEST_LENGTH+sizeof(unsigned int)/sizeof(unsigned char)),&currentblock->transactions->trans.extranonce,sizeof(unsigned int));	
	unsigned char *finalhash=(unsigned char*)malloc(SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	memset(finalhash,0,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	#ifdef DEBUG
	printf("%s\n\n%s\n\n%d %d\n\n%s\n\n",currentblock->blockheader.hashMerkleRoot,currentblock->blockheader.hashPrevBlock,currentblock->blockheader.Nonce,currentblock->transactions->trans.extranonce,input);
	#endif
	finalhash=SHA256(input,(2*SHA256_DIGEST_LENGTH+2*sizeof(unsigned int)/sizeof(unsigned char))*sizeof(unsigned char),finalhash);//find the hash using the fields stored in block
	unsigned char* check=(unsigned char*)malloc(SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	perform_hashing(currentblock->transactions,currentblock->transactionsCounter,check);//calculate MerkleRoot hash again
	unsigned char checkinput[2*SHA256_DIGEST_LENGTH+2*sizeof(unsigned int)/sizeof(unsigned char)];
	memcpy(checkinput,check,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	memcpy((checkinput+SHA256_DIGEST_LENGTH),currentblock->blockheader.hashPrevBlock,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	memcpy((checkinput+2*SHA256_DIGEST_LENGTH),&nonce,sizeof(unsigned int));
	memcpy((checkinput+2*SHA256_DIGEST_LENGTH+sizeof(unsigned int)/sizeof(unsigned char)),&extranonce,sizeof(unsigned int));
	unsigned char *finalcheck=(unsigned char*)malloc(SHA256_DIGEST_LENGTH*sizeof(unsigned char));//use the calculated hash to check the final hash
	memset(finalcheck,0,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	#ifdef DEBUG
	printf("%s\n\n%s\n\n%d %d\n\n%s\n\n",check,currentblock->blockheader.hashPrevBlock,nonce,extranonce,checkinput);
	#endif
	finalcheck=SHA256(checkinput,(2*SHA256_DIGEST_LENGTH+2*sizeof(unsigned int)/sizeof(unsigned char))*sizeof(unsigned char),finalcheck);
	int flag=1;
	memset(Bit,0,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	sprintf(Bit,"%d",currentblock->blockheader.Bits);
	int l,m=1;
	for(l=0;l<SHA256_DIGEST_LENGTH;l++) m=m&&(currentblock->blockheader.hashMerkleRoot[l]==check[l]);
	if(strcmp(finalcheck,finalhash)==0 && m && strcmp(finalhash,Bit)<0 && currentblock->transactions->trans.ammount==CURRENT_REWARD) flag=0; //check the validity of the given cash
	if(strcmp(finalcheck,finalhash)==0) printf("Final Hashes seem OK\n");
	if(m) printf("Merkle Hashes seem OK\n");
	if(strcmp(finalhash,Bit)<0) printf("Bit value seems OK\n");
	if(currentblock->transactions->trans.ammount==CURRENT_REWARD) printf("Reward seems OK\n");
	free(finalhash);
	free(finalcheck);
	return flag;//flag is 0 in case the block is valid
}

void Mining()
{
	if(pending_transactions->next==NULL)
	{
		printf("No reason for mining as there are no transactions to be commited\n");
		return;
	}
	struct block *newblock;
	int client,flag;
	int i;
	struct intlist *templist;
	struct transaction_list *templist2;
	struct info datainfo;
	struct transaction_list *temptrans;
	unsigned char finalinput[2*SHA256_DIGEST_LENGTH+2*sizeof(unsigned int)/sizeof(unsigned char)];
	unsigned char *finalhash;
	unsigned char Bit[SHA256_DIGEST_LENGTH];
	//******FILL NEW BLOCK*****************************************************************************//
	newblock=(struct block *)malloc(sizeof(struct block));
	newblock->magicNo=0xD9B4BEF9;	
	newblock->transactions=(struct transaction_list*)malloc(sizeof(struct transaction_list));
	newblock->transactions->trans.id_from=0;
	newblock->transactions->trans.id_to=id;//fill the necessary fields for new block
	newblock->transactions->trans.timestamp=time(NULL);	
	newblock->transactions->trans.extranonce=rand();//add the reward as first transaction
	newblock->transactions->trans.pubkey=pubkey;
	newblock->transactions->trans.ammount=CURRENT_REWARD;
	temptrans=newblock->transactions;
	newblock->transactionsCounter=1;
	while(newblock->transactionsCounter<MAX_TRANSACTIONS && pending_transactions->next!=NULL)
	{
		temptrans->next=pending_transactions->next;//add a number of transactions from the pending list in the block
		pending_transactions->next=pending_transactions->next->next;
		if(pending_transactions->next==NULL) newesttransaction=pending_transactions;
		temptrans=temptrans->next;
		temptrans->next=NULL;
		newblock->transactionsCounter++;
	}	
	newblock->blockheader.version=1;	
	memcpy(newblock->blockheader.hashPrevBlock,lastblock->blockheader.hashMerkleRoot,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	newblock->blockheader.Time=newblock->transactions->next->trans.timestamp;//fill the new block's header
	newblock->blockheader.Bits=current_target;
	newblock->blockheader.Nonce=0;
	newblock->Blocksize=sizeof(struct block)+newblock->transactionsCounter*sizeof(struct transaction);
	newblock->next=lastblock;
	//*************************************************************************************************//
	int kld=0;
	#ifdef DEBUG
	printf("Creating new block\n");
	#endif
	//********HASH NEW BLOCK***************************************************************************//
	finalhash=(unsigned char*)malloc(SHA256_DIGEST_LENGTH*sizeof(unsigned char));//necessary char matrices where input of hash is stored
	while(1)
	{
		#ifdef DEBUG
		printf("Trying hash number %d\n",++kld);
		#endif
		perform_hashing(newblock->transactions,newblock->transactionsCounter,newblock->blockheader.hashMerkleRoot);//find hash of transactions
		memcpy(finalinput,newblock->blockheader.hashMerkleRoot,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
		memcpy((finalinput+SHA256_DIGEST_LENGTH),newblock->blockheader.hashPrevBlock,SHA256_DIGEST_LENGTH*sizeof(unsigned char));				
		memcpy((finalinput+2*SHA256_DIGEST_LENGTH),&newblock->blockheader.Nonce,sizeof(unsigned int));		
		memcpy((finalinput+2*SHA256_DIGEST_LENGTH+(sizeof(unsigned int)/sizeof(unsigned char))),&newblock->transactions->trans.extranonce,sizeof(unsigned int));	//create a string including MerkleRoot,last block's hash, nonce and extranonce counters			
		finalhash=SHA256(finalinput,(2*SHA256_DIGEST_LENGTH+2*sizeof(unsigned int)/sizeof(unsigned char))*sizeof(unsigned char),finalhash);//find hash to check success
		memset(Bit,0,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
		sprintf(Bit,"%d",newblock->blockheader.Bits);
		if (strcmp(finalhash,Bit)<0) break;
		if (newblock->blockheader.Nonce==(1<<20))//in case of failure change nonce and extranonce counters accordingly and try again
		{
			newblock->blockheader.Nonce=0;
			newblock->transactions->trans.extranonce=rand();
		}
		else newblock->blockheader.Nonce++;
	}	
	free(finalhash);
	//*************************************************************************************************//
	
	//**********SEND DATA******************************************************************************//
	datainfo.type=3;
	datainfo.size=sizeof(struct block);
	memcpy(buffer,&datainfo,sizeof(struct info));
	templist=outsd;
	client=1;
	while(templist->next!=NULL)
	{
		if(templist->next==insd)
		{
			client=0;
			templist=templist->next;
			continue;
		}
		if(insist_write(templist->next->value, buffer, sizeof(struct info))<sizeof(struct info))
		{
			disconnectUser(client,templist);
		}
		else templist=templist->next;
	}
	memcpy(buffer,newblock,datainfo.size);
	templist=outsd;
	client=1;
	while(templist->next!=NULL)
	{
		if(templist->next==insd)
		{
			client=0;
			templist=templist->next;
			continue;
		}
		if(insist_write(templist->next->value, buffer, datainfo.size)<datainfo.size)
		{
			disconnectUser(client,templist);
		}
		else templist=templist->next;
	}
	templist=outsd;
	client=1;
	while(templist->next!=NULL)
	{
		if(templist->next==insd)
		{
			client=0;
			templist=templist->next;
			continue;
		}
		templist2=newblock->transactions;
		flag=0;
		for(i=0;i<newblock->transactionsCounter;i++)
		{
			memcpy(buffer,templist2,sizeof(struct transaction_list));
			if(insist_write(templist->next->value, buffer, sizeof(struct transaction_list))<sizeof(struct transaction_list))
			{
				flag=1;
				break;
			}
			else templist2=templist2->next;
		}
		if(flag) disconnectUser(client,templist);
		else templist=templist->next;
	}
	//*************************************************************************************************//
	#ifdef DEBUG
	printf("Inserting to chain\n");
	#endif
	//**********INSERT TO CHAIN************************************************************************//
	newblock->chaindifficulty=newblock->next->chaindifficulty+MAX_TARGET*1.0/newblock->blockheader.Bits;
	if(verify_block(newblock)==0)
	{
		struct blockedges *find=local_blocklist;
		while(find->next!=NULL)
		{
			if(find->next->blockAddress==lastblock)
			{
				newblock->next=find->next->blockAddress;
				find->next->blockAddress=newblock;
				lastblock=newblock;
				break;
			}
			else
			{
				find=find->next;
			}
		}
	}
	else
	{
		temptrans=newblock->transactions;
		for(i=0;i<newblock->transactionsCounter-1;i++)
		{
			temptrans=temptrans->next;	
		}
		temptrans->next=pending_transactions->next;
		pending_transactions->next=newblock->transactions->next;
		free(newblock);
	}
	//*************************************************************************************************//
}

void AddTransaction()
{
	int give_to,bitcoins,client;
	printf("Fill the node to give bitcoins to:");	
	scanf("%d",&give_to);
	printf("Select the ammount of bitcoint to give:");
	scanf("%d",&bitcoins);	
	struct transaction_list *newtrans;
	struct info datainfo;
	struct intlist *templist;
	//*****FILL TRANSACTION FIELDS********************************************************************//
	newtrans=(struct transaction_list *)malloc(sizeof(struct transaction_list));
	newtrans->trans.id_from=id;//fill the necessary fields in the new transaction
	newtrans->trans.id_to=give_to;
	newtrans->trans.extranonce=0;
	newtrans->trans.timestamp=time(NULL);
	newtrans->trans.ammount=bitcoins;
	newtrans->trans.pubkey=pubkey;
	newtrans->next=NULL;
	struct transaction_list *temptrans=newesttransaction;//add it to local transactions list
	temptrans->next=newtrans;
	newesttransaction=newtrans;
	//*************************************************************************************************//
	
	
	//****SEND DATA************************************************************************************//
	datainfo.type=4;
	datainfo.size=sizeof(struct transaction_list);
	memcpy(buffer,&datainfo,sizeof(struct info));
	templist=outsd;
	client=1;
	while(templist->next!=NULL)
	{
		if(templist->next==insd)
		{
			client=0;
			templist=templist->next;
			continue;
		}
		#ifdef DEBUG
		printf("SENDING\n");
		#endif
		if(insist_write(templist->next->value, buffer, sizeof(struct info))<sizeof(struct info))
		{
			disconnectUser(client,templist);	
		}
		else templist=templist->next;
	}
	memcpy(buffer,newtrans,datainfo.size);
	templist=outsd;
	client=1;
	while(templist->next!=NULL)
	{
		if(templist->next==insd)
		{
			client=0;
			templist=templist->next;
			continue;
		}
		if(insist_write(templist->next->value, buffer, datainfo.size)<datainfo.size)
		{
			disconnectUser(client,templist);
		}
		else templist=templist->next;
	}
	//*************************************************************************************************//
}

struct pidlist *envpid;
char pids[500];

void removeUser()
{
	envpid->next=NULL;
	pids[0]='\0';
	FILE *f;
	int pid=getpid();
	struct pidlist *templist;
	f=fopen("users","r");	
	fgets(pids,490,f);	
	fclose(f);	
    findpids(envpid,pids);//find the active users
    templist=envpid;
    struct pidlist* templist2;
    while(templist->next!=NULL)
    {
        if (templist->next->pid==pid)
        {
            templist2=templist->next;
            templist->next=templist->next->next;//remove itself from the list
            free(templist2);
            break;        
        }        
        templist=templist->next;
    }
    pidlistToString(pids,envpid);
	f=fopen("users","w");//resave the list
	fprintf(f,"%s",pids);	
	fclose(f);
    while(envpid->next!=NULL)	
    {
        templist=envpid->next;
        envpid->next=envpid->next->next;
        free(templist);    
    }
    free(envpid);
	//possibly check to make free if in case of SIGKILL handling
	exit(0);
}

struct block *findblock(char *hash)
{
	struct blockedges *finder=local_blocklist;
	struct block *checker;
	while(finder->next!=NULL)
	{
		checker=finder->next->blockAddress;
		while(checker!=NULL)
		{
			if(strncmp(checker->blockheader.hashMerkleRoot,hash,SHA256_DIGEST_LENGTH)==0) return checker;
			checker=checker->next;
		}
		finder=finder->next;
	}
	return NULL;
}

void ShowAccount()
{
	int i,remainder=wallet;
	struct blockedges *finder=local_blocklist;
	struct block *tempblock;
	struct transaction_list *temptrans;
	while(finder->next!=NULL)
	{
		tempblock=finder->next->blockAddress;
		while(tempblock!=NULL)
		{
			if(tempblock->checked==0)
			{
				temptrans=tempblock->transactions;
				for(i=0;i<tempblock->transactionsCounter;i++)
				{
					if(temptrans->trans.id_from==id) remainder-=temptrans->trans.ammount;
					if(temptrans->trans.id_to==id) remainder+=temptrans->trans.ammount;
					temptrans=temptrans->next;
				}
				tempblock->checked=1;
				tempblock=tempblock->next;
			}
			else break;
		}
		finder=finder->next;
	}
	printf("PID: %d Wallet Formal Remainder: %d\n",getpid(),remainder);
	temptrans=pending_transactions;
	while(temptrans->next!=NULL)
	{
		if(temptrans->next->trans.id_from==id) remainder-=temptrans->next->trans.ammount;
		if(temptrans->next->trans.id_to==id) remainder+=temptrans->next->trans.ammount;
		temptrans=temptrans->next;
	}
	printf("PID: %d Wallet Real Remainder: %d\n",getpid(),remainder);
	//pending:either print global variables or calculate by reading blockchain
}

struct transaction_list* readTransaction(int sd,char *buffer)
{
	int cnt=0;
	int away=0;
	int n;
	while (cnt<(int)sizeof(struct transaction_list))
	{
		n = read(sd, buffer+cnt, sizeof(struct transaction_list)-cnt);
		if (n <= 0) {
			if (n < 0)
				perror("read from remote peer failed");
			else	
				fprintf(stderr, "Peer went away\n");
			away=1;
			break;
		}
		cnt+=(int)n;
	}
	if(away) return newesttransaction;//maybe NULL for handling
	newesttransaction->next=(struct transaction_list*)malloc(sizeof(struct transaction_list));
	newesttransaction=newesttransaction->next;
	newesttransaction->next=NULL;
	memcpy(newesttransaction,buffer,sizeof(struct transaction_list));
	return newesttransaction;
}

struct block *readBlock(int sd,char *buffer)
{
	int cnt=0;
	int away=0;
	int i,n;
	struct block *newblock;
	struct transaction_list* temptrans;
	while (cnt<(int)sizeof(struct block))
	{
		n = read(sd, buffer+cnt, sizeof(struct block)-cnt);
		if (n <= 0) {
			if (n < 0)
				perror("read from remote peer failed");
			else	
				fprintf(stderr, "Peer went away\n");
			away=1;
			break;
		}
		cnt+=(int)n;
	}
	if(away) return NULL;
	newblock=(struct block *)malloc(sizeof(struct block));
	memcpy(newblock,buffer,sizeof(struct block));
	if(newblock->transactionsCounter==0) newblock->transactions=NULL;
	else newblock->transactions=(struct transaction_list*)malloc(sizeof(struct transaction_list));
	temptrans=newblock->transactions;
	for(i=0;i<newblock->transactionsCounter;i++)
	{
		cnt=0;
		away=0;
		while (cnt<(int)sizeof(struct transaction_list))
		{
			n = read(sd, buffer+cnt, sizeof(struct transaction_list)-cnt);
			if (n <= 0) {
				if (n < 0)
					perror("read from remote peer failed");
				else	
					fprintf(stderr, "Peer went away\n");
				away=1;
				break;
			}
			cnt+=(int)n;
		}
		if(away) break;
		if(i!=0)
		{
			temptrans->next=(struct transaction_list*)malloc(sizeof (struct transaction_list));
			temptrans=temptrans->next;
		}
		memcpy(temptrans,buffer,sizeof(struct transaction_list));
		temptrans->next=NULL;
	}
	if(!away) 
	{
		#ifdef DEBUG
		printf("%s\n%s",newblock->blockheader.hashMerkleRoot,newblock->blockheader.hashPrevBlock);
		#endif
		if(newblock->transactionsCounter==0) return newblock;
		if(verify_block(newblock)==0) 
		{
			temptrans=newblock->transactions;
			struct transaction_list *temptrans2,*temptrans3;
			while(temptrans!=NULL)
			{
				temptrans2=pending_transactions;
				while(temptrans2->next!=NULL)
				{
					if(temptrans2->next->trans.id_from==temptrans->trans.id_from && temptrans2->next->trans.id_to==temptrans->trans.id_to && temptrans2->next->trans.ammount==temptrans->trans.ammount && temptrans2->next->trans.timestamp==temptrans->trans.timestamp)
					{
							temptrans3=temptrans2->next;
							if(temptrans2->next->next==NULL) newesttransaction=temptrans2;
							temptrans2->next=temptrans2->next->next;
							free(temptrans3);
							break;
					}
					else temptrans2=temptrans2->next;
				}
				temptrans=temptrans->next;
			}
			return newblock;
		}	
		else
		{
			while(newblock->transactions!=NULL)
			{
				temptrans=newblock->transactions;
				newblock->transactions=newblock->transactions->next;
				free(temptrans);
			}
			free(newblock);
			return NULL;
		}
	}
	else
	{
		while(newblock->transactions!=NULL)
		{
			temptrans=newblock->transactions;
			newblock->transactions=newblock->transactions->next;
			free(temptrans);
		}
		free(newblock);
		return NULL;
	}
}

struct block *addBlock(int sd,char *buffer)
{
	
	int cnt=0;
	int away=0;
	int i,n;
	struct block *newblock;
	struct blockedges *finder;
	
	
	newblock=readBlock(sd,buffer);
	if(newblock==NULL) 
	{
		return NULL;				
	}
	else
	{
		finder=local_blocklist;
		while(finder->next!=NULL)
		{
			if(strncmp(finder->next->blockAddress->blockheader.hashMerkleRoot,newblock->blockheader.hashPrevBlock,SHA256_DIGEST_LENGTH)!=0)
			{
				finder=finder->next;
			}
			else
			{
				newblock->next=finder->next->blockAddress;
				finder->next->blockAddress=newblock;
				newblock->chaindifficulty=newblock->next->chaindifficulty+MAX_TARGET*1.0/newblock->blockheader.Bits;
				if(newblock->chaindifficulty>lastblock->chaindifficulty) lastblock=newblock;
				break;
			}
		}
		if(finder->next==NULL)
		{
			finder->next=(struct blockedges*)malloc(sizeof(struct blockedges));
			finder=finder->next;
			finder->next=NULL;
			finder->blockAddress=newblock;
			newblock->next=findblock(newblock->blockheader.hashPrevBlock);
			newblock->chaindifficulty=newblock->next->chaindifficulty+MAX_TARGET*1.0/newblock->blockheader.Bits;
			if(newblock->chaindifficulty>lastblock->chaindifficulty) lastblock=newblock;	
		}
		return newblock;
	}
	return NULL;
}

void clearChecked()
{
	struct blockedges *finder=local_blocklist;
	struct block *tempblock;
	while(finder->next!=NULL)
	{
		tempblock=finder->next->blockAddress;
		while(tempblock!=NULL)
		{
			tempblock->checked=0;
			tempblock=tempblock->next;
		}
		finder=finder->next;
	}
}

void errorExit()
{
	printf("Problems appeared during blockchain and transactions update of %d. Please rerun\n",getpid());
	exit(1);
}

void showTransactions()
{
	struct transaction_list *temptrans=pending_transactions;
	int i=1;
	if(temptrans->next==NULL)
	{
		printf("No Pending Transactions at the moment\n");
		return;
	}
	while(temptrans->next!=NULL)
	{
		printf("Transaction %d\n",i++);
		printf("From: %d, To: %d, Ammount: %d\n",temptrans->next->trans.id_from,temptrans->next->trans.id_to,temptrans->next->trans.ammount);
		printf("\n");
		temptrans=temptrans->next;
	}
}

void showBlockChain()
{
	struct blockedges *finder=local_blocklist;
	struct block *tempblock;
	int i=1;
	while(finder->next!=NULL)
	{
		tempblock=finder->next->blockAddress;
		while(tempblock!=NULL)
		{
			if(tempblock->checked==0)
			{
				printf("Block %d\n",i++);
				printf("Current Hash: %s\n",tempblock->blockheader.hashMerkleRoot);
				printf("previous Hash: %s\n",tempblock->blockheader.hashPrevBlock);
				tempblock->checked=1;
				tempblock=tempblock->next;
			}
			else break;
		}
		finder=finder->next;
	}
}

int main(void)
{
	//*****************DECLARATIONS********************************************************************//
	int i,j;
	int client;
	struct info datainfo;
	struct blockedges* finder;
	char addrstr[INET_ADDRSTRLEN];
	int ready,choice;
	ssize_t n;
	socklen_t len;
	fd_set read_from;
	struct transaction_list* temptrans;
	int stdin_fd=fileno(stdin),stdout_fd=fileno(stdout);
	FILE *f;
    int pid=getpid();
	struct pidlist *templist;
	struct block *tempblock;
	int blocksnum,edgesnum;
	int transnum;
	struct sockaddr_in outsa[number_pids];
	struct sockaddr_in insa;
	int sd,print=1;
	struct intlist *currentsd,*lastsd;
	char hostname[100];
	struct block* newblock;
	struct hostent *hp;
	struct timeval timeout;
	fd_set new_servers;
	int port;
	int cnt,away;
	//*************************************************************************************************//
	
	
	//************************INITIATIONS**************************************************************//
	srand(time(NULL));
	maxsd=0;
	id=pid;
	current_target=1<<15;
	id=getpid();
	pending_transactions=(struct transaction_list *)malloc(sizeof(struct transaction_list));//transactions not yet included in a block
	pending_transactions->next=NULL;
	newesttransaction=pending_transactions;
	local_blocklist=(struct blockedges *)malloc(sizeof(struct blockedges));//local version of the public blocklist
	local_blocklist->next=NULL;	//the first node of these lists does not contain data and exists only to represent the list head
	lastblock=NULL;	
	wallet=rand()%200+1;
	/* Make sure a broken connection doesn't kill us */
	signal(SIGPIPE, SIG_IGN);

	signal(SIGKILL,removeUser);//remove user from active users list on exit
	envpid=NULL;
    envpid=(struct pidlist*)malloc(sizeof(struct pidlist));
    envpid->next=NULL;
	//*************************************************************************************************//
	
	
	//********ACTIVE USERS*****************************************************************************//
	pids[0]='\0';
	f=fopen("users","r");//file where a list of active users is stored	
	fgets(pids,490,f);	
	fclose(f);	
	if(strlen(pids)!=0)
    {
        findpids(envpid,pids);
    }
	if(envpid->next==NULL) centralpid=getpid();
	else centralpid=envpid->next->pid;
    addpid(pids,pid);//add itself in the list
	f=fopen("users","w");
	fprintf(f,"%s",pids);	
	fclose(f);
    
	//connect to users already online
	//act as servers for users logging in afterwards
	/* Create TCP/IP socket, used as main chat channel */	
	
	number_pids=0;
	templist=envpid;
	while(templist->next!=NULL)
	{
		number_pids++;//count number of connects to perform as client
		templist=templist->next;
	}
	//*************************************************************************************************//
	
	
	//***********CONNECT CLIENT SIDE*******************************************************************//
	print=1;
	outsd=(struct intlist *)malloc(sizeof(struct intlist));//the two lists are connected with a node in the middle 
	insd=(struct intlist *)malloc(sizeof(struct intlist));//stating the start of the insd list
	currentsd=outsd;
	lastsd=insd;
	outsd->next=insd;
	insd->next=NULL;
	strcpy(hostname,"localhost");//all nodes are local
	templist=envpid;	
	for(i=0;i<number_pids;i++)
	{	
		port=TCP_PORT(templist->next->pid);
		currentsd->next=(struct intlist *)malloc(sizeof(struct intlist));
		currentsd=currentsd->next;
		currentsd->next=insd;
		/* Create TCP/IP socket, used as main chat channel */
		if ((currentsd->value = socket(PF_INET, SOCK_STREAM, 0)) < 0) 
		{ //create socket for each connection
			perror("socket");
			exit(1);
		}
		fprintf(stderr, "Created TCP socket\n");
		/* Look up remote hostname on DNS */
		if ( !(hp = gethostbyname(hostname))) 
		{
			printf("DNS lookup failed for host %s\n", hostname);
			exit(1);
		}
		/* Connect to remote TCP port */
		outsa[i].sin_family = AF_INET;
		outsa[i].sin_port = htons(port);
		memcpy(&outsa[i].sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
		fprintf(stderr, "Connecting to remote host... ");	fflush(stderr);
		if (connect(currentsd->value, (struct sockaddr *) &outsa[i], sizeof(outsa[i])) < 0)
		{//connect to the active user
			perror("connect");
			exit(1);
		}
		if(currentsd->value>maxsd) maxsd=currentsd->value;//********!!!!!!!!!!
		#ifdef DEBUG
		printf("Current pid=%d\n",templist->next->pid);
		#endif
		if(templist->next->pid==centralpid)
		{
			cnt=0;
			away=0;
			while (cnt<(int)sizeof(int))
			{
				n = read(currentsd->value, buffer+cnt, sizeof(int)-cnt);
				if (n <= 0) 
				{
					if (n < 0)
						perror("read from remote peer failed");
					else	
						fprintf(stderr, "Peer went away\n");
					away=1;
					break;
				}
				cnt+=(int)n;
			}
			if(!away)
			{
				memcpy(&edgesnum,buffer,sizeof(int));
				#ifdef DEBUG
				printf("Received Number of Blocks\n");
				#endif
				finder=local_blocklist;
				for(i=0;i<edgesnum;i++)
				{
					cnt=0;
					while (cnt<(int)sizeof(int))
					{
						n = read(currentsd->value, buffer+cnt, sizeof(int)-cnt);
						if (n <= 0) 
						{
							if (n < 0)
								perror("read from remote peer failed");
							else	
								fprintf(stderr, "Peer went away\n");
							away=1;
							break;
						}
						cnt+=(int)n;
					}
					if(away) break;
					else
					{
						memcpy(&blocksnum,buffer,sizeof(int));
						finder->next=(struct blockedges*)malloc(sizeof(struct blockedges));
						for(j=0;j<blocksnum;j++)
						{
							if(j==0) 
							{
								finder->next->blockAddress=readBlock(currentsd->value,buffer);
								if(finder->next->blockAddress==NULL)
								{
									away=1;
									break;
								}
								tempblock=finder->next->blockAddress;
								if(strcmp(tempblock->blockheader.hashPrevBlock,"")==0) tempblock->next=NULL;
							}
							else
							{
								tempblock->next=readBlock(currentsd->value,buffer);
								if(tempblock->next==NULL)
								{
									away=1;
									break;
								}
								tempblock=tempblock->next;
								if(strcmp(tempblock->blockheader.hashPrevBlock,"")==0) tempblock->next=NULL;
							}
						}
						if(away) break;
						if(tempblock->next!=NULL) tempblock->next=findblock(tempblock->blockheader.hashPrevBlock);
						if(lastblock==NULL) lastblock=finder->next->blockAddress;
						else 
						{
							if(lastblock->chaindifficulty<finder->next->blockAddress->chaindifficulty) lastblock=finder->next->blockAddress;
						}
						finder=finder->next;
						finder->next=NULL;
					}
				}
			}
			if(away)
			{
				//free blocks read
				errorExit();
			}
			cnt=0;
			while (cnt<(int)sizeof(int))
			{
				n = read(currentsd->value, buffer+cnt, sizeof(int)-cnt);
				if (n <= 0) 
				{
					if (n < 0)
						perror("read from remote peer failed");
					else	
						fprintf(stderr, "Peer went away\n");
					away=1;
					break;
				}
				cnt+=(int)n;
			}
			#ifdef DEBUG
			printf("Received Number of Transactions\n");
			#endif
			if(!away)
			{
				memcpy(&transnum,buffer,sizeof(int));
				for(i=0;i<transnum;i++)
				{
					newesttransaction=readTransaction(currentsd->value,buffer);
					if(newesttransaction==NULL)
					{
						away=1;
						break;
					}
				}
				if(away)
				{
					//free transactions read
					errorExit();
				}
			}
			else errorExit();
		}
		templist=templist->next;
	}
//*************************************************************************************************//
	while(envpid->next!=NULL)	
    {
        templist=envpid->next;
        envpid->next=envpid->next->next;//free pid list
        free(templist);    
    }
	if(number_pids==0)
	{
		local_blocklist->next=(struct blockedges*)malloc(sizeof(struct blockedges));
		local_blocklist->next->blockAddress=(struct block*)malloc(sizeof(struct block));
		local_blocklist->next->next=NULL;
		local_blocklist->next->blockAddress->magicNo=0xD9B4BEF9;
		local_blocklist->next->blockAddress->transactionsCounter=0;
		local_blocklist->next->blockAddress->transactions=NULL;
		local_blocklist->next->blockAddress->blockheader.version=1;
		local_blocklist->next->blockAddress->blockheader.Time=time(NULL);
		local_blocklist->next->blockAddress->blockheader.Nonce=0;
		local_blocklist->next->blockAddress->Blocksize=sizeof(struct block)+local_blocklist->next->blockAddress->transactionsCounter*sizeof(struct transaction);
		local_blocklist->next->blockAddress->blockheader.Bits=1;
		local_blocklist->next->blockAddress->checked=0;
		local_blocklist->next->blockAddress->chaindifficulty=0;		
		strcpy(local_blocklist->next->blockAddress->blockheader.hashPrevBlock,"");
		//strcpy(local_blocklist->next->blockAddress->blockheader.hashMerkleRoot,FIRST_HASH);
		perform_hashing(local_blocklist->next->blockAddress->transactions,local_blocklist->next->blockAddress->transactionsCounter,local_blocklist->next->blockAddress->blockheader.hashMerkleRoot);
		lastblock=local_blocklist->next->blockAddress;
	}
	number_inpids=0;
	
	
	//********LISTEN TO CONNECTIONS*******************************************************************//
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) 
	{//create socket to listen from
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");

	/* Bind to a well-known port */
	memset(&insa, 0, sizeof(insa));
	insa.sin_family = AF_INET;
	insa.sin_port = htons(TCP_PORT(id));
	insa.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sd, (struct sockaddr *)&insa, sizeof(insa)) < 0) 
	{ //bind a socket to an address and port pair
		perror("bind");
		exit(1);
	}
	fprintf(stderr, "Bound TCP socket to port %d\n", TCP_PORT(id));

	/* Listen for incoming connections */
	if (listen(sd, TCP_BACKLOG) < 0) 
	{
		perror("listen");
		exit(1);
	}
	//*************************************************************************************************//
	
	
	
	while(1)
	{
		//**********CONNECT SERVER SIDE********************************************************************//
		timeout.tv_sec=1;
		timeout.tv_usec=0;
		FD_ZERO(&new_servers);
		FD_SET(sd,&new_servers);
		if(select(sd+1,&new_servers,NULL,NULL,&timeout)<0)
		{
			perror("select");
			exit(1);
		}
		if(FD_ISSET(sd,&new_servers))
		{
			lastsd->next=(struct intlist *)malloc(sizeof(struct intlist));
			lastsd=lastsd->next;
			lastsd->next=NULL;
			len = sizeof(struct sockaddr_in);
			if ((lastsd->value = accept(sd, (struct sockaddr *)&insa, &len)) < 0) {
				perror("accept");
				exit(1);
			}
			if(lastsd->value>maxsd) maxsd=lastsd->value;//!!!!!!!!!!!!!!!
			number_inpids++;
			if (!inet_ntop(AF_INET, &insa.sin_addr, addrstr, sizeof(addrstr))) {
				perror("could not format IP address");
				exit(1);
			}
			fprintf(stderr, "Incoming connection from %s:%d\n",
				addrstr, ntohs(insa.sin_port));
			if(getpid()==centralpid)
			{
				away=0;
				finder=local_blocklist;
				edgesnum=0;
				while(finder->next!=NULL)
				{
					edgesnum++;
					finder=finder->next;
				}
				finder=local_blocklist;
				memcpy(buffer,&edgesnum,sizeof(int));
				if(insist_write(lastsd->value,buffer,sizeof(int))<=0) away=1;
				#ifdef DEBUG
				printf("Sent Number of Blocks\n");
				#endif
				clearChecked();
				while(finder->next!=NULL && !away)
				{
					tempblock=finder->next->blockAddress;
					blocksnum=0;
					while(tempblock!=NULL)
					{
						if(tempblock->checked==0)
						{
							blocksnum++;
							tempblock=tempblock->next;
						}
						else break;
					}
					memcpy(buffer,&blocksnum,sizeof(int));
					if(insist_write(lastsd->value,buffer,sizeof(int))<=0) away=1;
					if(away) break;
					tempblock=finder->next->blockAddress;
					while(tempblock!=NULL)
					{
						if(tempblock->checked==0)
						{
							memcpy(buffer,tempblock,sizeof(struct block));
							if(insist_write(lastsd->value,buffer,sizeof(struct block))<=0) away=1;
							tempblock->checked=1;
							temptrans=tempblock->transactions;
							for(i=0;i<tempblock->transactionsCounter;i++)
							{
								memcpy(buffer,temptrans,sizeof(struct transaction_list));
								if(insist_write(lastsd->value,buffer,sizeof(struct transaction_list))<=0) away=1;
								if(away) break;
								temptrans=temptrans->next;
							}
							tempblock=tempblock->next;
						}
						else break;
						if(away) break;
					}
					if(away) break;
					finder=finder->next;
				}
				#ifdef DEBUG
				printf("Sent Blocks\n");
				#endif
				if(!away)
				{
					temptrans=pending_transactions;
					transnum=0;
					while(temptrans->next!=NULL)
					{
						transnum++;
						temptrans=temptrans->next;
					}
					temptrans=pending_transactions;
					memcpy(buffer,&transnum,sizeof(int));
					if(insist_write(lastsd->value,buffer,sizeof(int))<=0) away=1;
					#ifdef DEBUG
					printf("Sent Number of Transactions\n");
					#endif
					if(!away)
					{
						while(temptrans->next!=NULL)
						{
							memcpy(buffer,temptrans->next,sizeof(struct transaction_list));
							if(insist_write(lastsd->value,buffer,sizeof(struct transaction_list))<=0) away=1;
							if(away) break;
							temptrans=temptrans->next;
						}
					}
					#ifdef DEBUG
					printf("Sent Transactions\n");
					#endif
				}
			}
		}
		//*************************************************************************************************//
		if(maxsd<stdin_fd) maxsd=stdin_fd;
		
		//******CHECK FOR INPUTS***************************************************************************//
		if(print)
		{
			printf("Please choose one of the following:\n");
			printf("0:See account details\n");
			printf("1:Add new transaction\n");
			printf("2:Mine new block\n");
			printf("3:Show Blockchain\n");
			printf("4:Show Pending Transactions\n");
			print=0;
			printf("PID:%d\n",id);
		}
		FD_ZERO(&read_from);
		FD_SET(stdin_fd,&read_from);
		currentsd=outsd;
		while(currentsd->next!=NULL)
		{
			if(currentsd->next==insd)
			{
				currentsd=currentsd->next;
				continue;
			}
			FD_SET(currentsd->next->value,&read_from);
			currentsd=currentsd->next;
		}
		timeout.tv_sec=1;
		timeout.tv_usec=0;
		select(maxsd+1,&read_from,NULL,NULL,&timeout);
		if(FD_ISSET(stdin_fd,&read_from))
		{
			scanf("%d",&choice);
			if(choice==0)
			{
				clearChecked();
				ShowAccount();
			}	
			else if(choice==1)
			{
				AddTransaction();
			}	
			else if(choice==2)
			{
				Mining();
				#ifdef DEBUG
				printf("It is done\n");
				#endif
			}
			else if(choice==3)
			{
				clearChecked();
				showBlockChain();
			}
			else if(choice==4)
			{
				showTransactions();	
			}
			else
			{
				printf("Sorry, Invalid Choice\n");
			}
			print=1;
		}
		currentsd=outsd;
		client=1;
		while(currentsd->next!=NULL)
		{
			if(currentsd->next==insd)
			{
				client=0;
				currentsd=currentsd->next;
				continue;
			}
			if(FD_ISSET(currentsd->next->value,&read_from))
			{
				cnt=0;
				away=0;
				while (cnt<(int)sizeof(struct info))
				{
					n = read(currentsd->next->value, buffer+cnt, sizeof(struct info)-cnt);
					if (n <= 0) {
						if (n < 0)
							perror("read from remote peer failed");
						else	
							fprintf(stderr, "Peer went away\n");
						away=1;
						break;
					}
					cnt+=(int)n;
				}
				if(away) break;
				memcpy(&datainfo,buffer,sizeof(struct info));
				#ifdef DEBUG
				printf("Trying data\n");
				#endif
				if(datainfo.type==3)
				{
					newblock=addBlock(currentsd->next->value,buffer);	
				}
				else if(datainfo.type==4)
				{
					#ifdef DEBUG
					printf("RECEIVING\n");
					#endif
					newesttransaction=readTransaction(currentsd->next->value,buffer);
				}
				//break;
			}
			currentsd=currentsd->next;
		}
		//*************************************************************************************************//
		if(away) disconnectUser(client,currentsd);
	}
	
	//repeat until exiting

	removeUser();
}
