#include<stdio.h>
#include<math.h>
#include<time.h>
#include<stdlib.h>
#include<string.h>

#define SAMPLE 1000 
//*********************************************************************
//katastash 0 den uparxoun xrhstes
//katastaseis 1 - k pou den eksuphretei o b.
//katastash (k+1) eksuphretei mono o b
//katastaseis (k+2) - (2k) eksuphretoun kai oi 2 alla oi xrhstes pleon dialegoun mono ton a
//katastaseis (2k+1) - (10+k) eksuphretoun kai oi 2 kai oi xrhstes dialegoun kai tous 2 eksuphrethtes
//*********************************************************************
//oi metabaseis apo katastash se katastash fainontai pio analutika sto diagramma katastasewn
//opou oi katastaseis 1 - k orizontai ws 1a - ka
//kai oi katastaseis (k+1) - 2k orizontai ws 1b - kb
//enw oi katastaseis (2k+1) - (10+k) orizontai ws (k+1) - 10
//h katastash 0 orizetai ws 0
//*********************************************************************
int main(int argc, char **argv)
{
	if (argc<3)
	{
		printf("Usage: prosomoiosh <k> <lamda>\n");
		exit(1);
	}
	int k=atoi(argv[1]);
	if (k<1 || k>9)
	{
		printf("<k> out of bounds\n");
		exit(1);
	}
	int i;
	int lamda=atoi(argv[2]);
	int max_state=10+k;   
	double p[11+k];
	for(i=0;i<11+k;i++) p[i]=0.0;
	int current_state=0;
	int stable_state=2*k+1;
	int existing_clients=0;
	double margin1=0.0,margin2=0.0;
	int m_a=4;
	int m_b=1;
	srand(time(NULL));
	double luck;
	double prev_avg=-1;
	double avg=0;
	int current_time=0;
	int arrivals=0;
	int arrival[11+k];
	for(i=0;i<11+k;i++) arrival[i]=0;
	int counter=0;
	while(fabs(avg-prev_avg)>=0.001 || counter<SAMPLE)
	{
		if(fabs(avg-prev_avg)>=0.001) counter=0;
		else counter++; //metrame to plhthos twn sunexomenwn forwn pou exoume sxedon idio meso oro
		if (current_state==0)
		{
			arrival[current_state]++;			
			current_state++; //prosthetoume ton 1o xrhsth
			existing_clients++;
			current_time++;
			arrivals++;
		}
		else
		{
			if(current_state<=k)  //kathorizoume to diasthma epilogwn wste na epilegontai mono apodektes energeies gia epomenh katastash
			{
				margin1=1.0*lamda/(lamda+m_a);
				margin2=1.0;
			}
			else if (current_state==k+1)
			{
				margin1=1.0*lamda/(lamda+m_b);
				margin2=1.0*lamda/(lamda+m_b);
			}
			else if (current_state<10+k)
			{
				margin1=1.0*lamda/(lamda+m_a+m_b);
				margin2=1.0*(lamda+m_a)/(lamda+m_a+m_b);
			}
			else
			{
				margin1=1.0*lamda/(lamda+m_a+m_b);
				margin2=1.0*(lamda+m_a)/(lamda+m_a+m_b);
			}
			luck=(float)rand()/(float)RAND_MAX;//epilegoume epomenh energeia
			if(luck<=margin1)
			{
				arrival[current_state]++;				
				if(current_state<k) current_state++; //prosthetoume neo xrhsth
				else if (current_state==k) current_state+=k+1;
				else if (current_state<(k+10)) current_state++;
				if (current_state<(k+10)) existing_clients++;
				current_time++;
				arrivals++;
			}
			else if (luck<=margin2)
			{
				if(current_state<=k) current_state--; //o a eksuphretei xrhsth
				else if (current_state>(k+1)) current_state--;
				existing_clients--;
				current_time++;
			}
			else 
			{
				if(current_state>2*k+1) current_state--; //o b eksuphretei xrhsth
				else if (current_state>k) current_state-=k+1;
				existing_clients--;
				current_time++;
			}
		}
		prev_avg=avg;
		for(i=0;i<11+k;i++) p[i]=1.0*arrival[i]/arrivals;
		avg=0.0;
		for(i=0;i<11+k;i++) avg+=i*p[i];//vriskoume ton neo meso oro
		if(current_time%SAMPLE==0) printf("p[%d] = %lf\n",current_time/SAMPLE,avg);//kanoume deigmatolhpsia stous mesous orous
	}
	printf("Final average:\n");
	printf("p[final] = %lf\n",avg);
	printf("Overall simulation duration:%d sec\n",current_time);
	printf("\n\nProbability of each state:\n");
	for (i=0;i<11+k;i++) printf("p[%d] = %lf\n",i,1.0*p[i]);//vriskoume kanonikopoihmenous xronous se kathe katastash
	double gamma_a=0.0,gamma_b=0.0,gamma=0.0;
	gamma_a=m_a*(1.0-1.0*p[0]-1.0*p[k+1]);//vriskoume apodosh tou a
	gamma_b=m_b*(1.0-1.0*p[0]);	//vriskoume apodosh tou b
	for (i=1;i<=k;i++) gamma_b-=m_b*1.0*p[i];
	gamma=gamma_a+gamma_b;
	printf("\ngamma_a=%lf \ngamma_b=%lf \ngamma=%lf\n",gamma_a, gamma_b, gamma);
	printf("gamma_a/gamma_b = %lf\n",1.0*gamma_a/gamma_b);
	return 0;
}
