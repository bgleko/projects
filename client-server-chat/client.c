/*
 * socket-client.c
 * Simple TCP/IP communication using sockets
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 */

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>
 
#include <fcntl.h>

#include "socket-common.h"

#include <sys/ioctl.h>

#include <sys/stat.h>

#include <crypto/cryptodev.h>

#define DATA_SIZE       256
#define BLOCK_SIZE      16
#define KEY_SIZE	16  /* AES128 */

#if defined(DEBUG)
# define DBG 1
#else
# define DBG 0
#endif


/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;
	
	while (cnt > 0) {
	        ret = write(fd, buf, cnt);
	        if (ret < 0)
	                return ret;
	        buf += ret;
	        cnt -= ret;
	}

	return orig_cnt;
}

int main(int argc, char *argv[])
{
	int sd, port;
	ssize_t n;
	unsigned char buf[DATA_SIZE];
	char *hostname;
	struct hostent *hp;
	struct sockaddr_in sa;
	fd_set read_from;
	int stdin_fd=fileno(stdin),stdout_fd=fileno(stdout);
	int ready;
	if (argc != 3) {
		fprintf(stderr, "Usage: %s hostname port\n", argv[0]);
		exit(1);
	}
	struct session_op sess;
	struct crypt_op cryp;
	struct {
		unsigned char 	in[DATA_SIZE],
				encrypted[DATA_SIZE],
				decrypted[DATA_SIZE],
				iv[BLOCK_SIZE],
				key[KEY_SIZE];
	} data;
	int cfd;
	hostname = argv[1];
	port = atoi(argv[2]); /* Needs better error checking */

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");
	
	/* Look up remote hostname on DNS */
	if ( !(hp = gethostbyname(hostname))) {
		printf("DNS lookup failed for host %s\n", hostname);
		exit(1);
	}

	/* Connect to remote TCP port */
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	memcpy(&sa.sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
	fprintf(stderr, "Connecting to remote host... "); fflush(stderr);
	if (connect(sd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
		perror("connect");
		exit(1);
	}
	fprintf(stderr, "Connected.\n");
	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));
	cfd = open("/dev/crypto", O_RDWR);
        if (cfd < 0) {
                perror("open(/dev/crypto)");
                return 1;
        }
	strncpy(data.key,"abcdefghijklmnop",KEY_SIZE);
	strncpy(data.iv,"qrstuvwxyzabcdef", BLOCK_SIZE);
		
	
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;
	sess.key = data.key;
	
		
	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		return 1;
	}
	cryp.ses = sess.ses;
        cryp.len = sizeof(data.in);
        cryp.iv = data.iv;
        cryp.src = data.in;
        cryp.dst = data.encrypted;
        cryp.op = COP_ENCRYPT;
	int i;
	for (;;) {
		FD_ZERO(&read_from);
		FD_SET(stdin_fd,&read_from);
		FD_SET(sd,&read_from);
		if ((ready=select(sd+1,&read_from,NULL,NULL,NULL))<0)
		{
			perror("select");
			exit(1);
		}
		if (FD_ISSET(sd,&read_from))
		{
			int away=0;
			int cnt=0;
			while (cnt<(int)sizeof(buf)){
				n = read(sd, buf+cnt, sizeof(buf)-cnt);
				if (n <= 0) {
					if (n < 0)
						perror("read from remote peer failed");
					else	
						fprintf(stderr, "Peer went away\n");
					away=1;
					break;
				}
				if(DBG){
					printf("OK-(%d)\n",(int)n);
					for(i=0; i<(int)n;i++)
					printf("%x",buf[cnt+i]);
				}
				cnt+=(int)n;
			}
			if(away) break;
//			char addr[]=hostname;
			insist_write(stdout_fd,"      (server):",strlen("      (server):"));
			if(DBG){ for(i=0; i<DATA_SIZE;i++) data.encrypted[i]=buf[i];}

			memcpy(data.encrypted,buf,DATA_SIZE);
			cryp.src = data.encrypted;
			cryp.dst = data.decrypted;
			cryp.op = COP_DECRYPT;
			if (ioctl(cfd, CIOCCRYPT, &cryp)) {
				perror("ioctl(CIOCCRYPT)");
				return 1;
			}
//			memset(&buf[0],0,sizeof(buf));
			memcpy(buf,data.decrypted,DATA_SIZE);
			if (insist_write(stdout_fd, buf, strlen(buf)) != strlen(buf)) {
				perror("write to remote peer failed");
				break;
			}
		}
		else if (FD_ISSET(stdin_fd,&read_from))
		{
			memset(&buf[0],0,DATA_SIZE);
			n = read(stdin_fd, buf, DATA_SIZE-1);
			if (n <= 0) {
				if (n < 0)
					perror("read from remote peer failed");
				else	
					fprintf(stderr, "Peer went away\n");
				break;
			}
			buf[n]='\0';
			memcpy(data.in,buf,n+1);
			if(DBG) printf("ME:%s",data.in);
			data.in[n]='\0';
			cryp.src = data.in;
			cryp.dst = data.encrypted;
			cryp.op = COP_ENCRYPT;
			if (ioctl(cfd, CIOCCRYPT, &cryp)) {
				perror("ioctl(CIOCCRYPT)");
				return 1;
			}
			memset(&buf[0],0,sizeof(buf));
			memcpy(buf,data.encrypted,DATA_SIZE);
			int i;
//			for(i=0;i<DATA_SIZE;i++)
//				buf[i]=data.encrypted[i];
			if (insist_write(sd, buf, DATA_SIZE) != DATA_SIZE) {
				perror("write to remote peer failed");
				break;
			}
		}
	}
	/*
	 * Let the remote know we're not going to write anything else.
	 * Try removing the shutdown() call and see what happens.
	 */
	if (ioctl(cfd, CIOCFSESSION, &sess.ses)) {
			perror("ioctl(CIOCFSESSION)");
			return 1;
	}
	
	if (shutdown(sd, SHUT_WR) < 0) {
		perror("shutdown");
		exit(1);
	}
	if (close(cfd) < 0)
		perror("close");
	
	
	fprintf(stderr, "\nDone.\n");
	return 0;
}
