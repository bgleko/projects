/*
 * socket-server.c
 * Simple TCP/IP communication using sockets
 *
 */

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include "socket-common.h"

#include <fcntl.h>

#include <sys/ioctl.h>
 
#include <sys/stat.h>
#include <crypto/cryptodev.h>


#define DATA_SIZE       256
#define BLOCK_SIZE      16
#define KEY_SIZE	16  /* AES128 */

#if defined(DEBUG)
# define DBG 1
#else
# define DBG 0
#endif

/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;
	
	while (cnt > 0) {
	        ret = write(fd, buf, cnt);
	        if (ret < 0)
	                return ret;
	        buf += ret;
	        cnt -= ret;
	}

	return orig_cnt;
}

int main(void)
{
	char buf[DATA_SIZE];
	char addrstr[INET_ADDRSTRLEN];
	int sd, newsd;
	int ready;
	ssize_t n;
	socklen_t len;
	fd_set read_from;
	struct sockaddr_in sa;
	struct session_op sess;
	struct crypt_op cryp;
	int stdin_fd=fileno(stdin),stdout_fd=fileno(stdout);
	struct {
		unsigned char 	in[DATA_SIZE],
				encrypted[DATA_SIZE],
				decrypted[DATA_SIZE],
				iv[BLOCK_SIZE],
				key[KEY_SIZE];
	} data;
	int cfd;
	
	/* Make sure a broken connection doesn't kill us */
	signal(SIGPIPE, SIG_IGN);

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");
	
	/* Bind to a well-known port */
	memset(&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(TCP_PORT);
	sa.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
		perror("bind");
		exit(1);
	}
	fprintf(stderr, "Bound TCP socket to port %d\n", TCP_PORT);

	/* Listen for incoming connections */
	if (listen(sd, TCP_BACKLOG) < 0) {
		perror("listen");
		exit(1);
	}
	int i;
	/* Loop forever, accept()ing connections */
	for (;;) {
		fprintf(stderr, "Waiting for an incoming connection...\n");

		/* Accept an incoming connection */
		len = sizeof(struct sockaddr_in);
		if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
			perror("accept");
			exit(1);
		}
		if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
			perror("could not format IP address");
			exit(1);
		}
		fprintf(stderr, "Incoming connection from %s:%d\n",
			addrstr, ntohs(sa.sin_port));
		char addr[100];
		int len=0;
		strcpy(addr+len,"      (");
		len+=8;
		strcpy(addr+len,addrstr);
		len+=strlen(addrstr);
		strcpy(addr+len,":");
		len+=1;
		sprintf(addr+len, "%d", ntohs(sa.sin_port));
		len+=5;
		strcpy(addr+len, "):");
		len+=2;
		addr[len]='\0';
		memset(&sess, 0, sizeof(sess));
		memset(&cryp, 0, sizeof(cryp));
		cfd = open("/dev/crypto", O_RDWR);
      		if (cfd < 0) {
               		 perror("open(/dev/crypto)");
        	        return 1;
	        }

		strncpy(data.key,"abcdefghijklmnop",KEY_SIZE);
		strncpy(data.iv,"qrstuvwxyzabcdef", BLOCK_SIZE);
		
		sess.cipher = CRYPTO_AES_CBC;
		sess.keylen = KEY_SIZE;
		sess.key = data.key;
		
		
		if (ioctl(cfd, CIOCGSESSION, &sess)) {
			perror("ioctl(CIOCGSESSION)");
			return 1;
		}
		cryp.ses = sess.ses;
                cryp.len = sizeof(data.in);
                cryp.iv = data.iv;
                cryp.src = data.in;
                cryp.dst = data.encrypted;
                cryp.op = COP_ENCRYPT;

		/* We break out of the loop when the remote peer goes away */
		for (;;) {
			FD_ZERO(&read_from);
			FD_SET(stdin_fd,&read_from);
			FD_SET(newsd,&read_from);
			if ((ready=select(newsd+1,&read_from,NULL,NULL,NULL))<0)
			{
				perror("select");
				exit(1);
			}
			if (FD_ISSET(newsd,&read_from))
			{	
				int away=0;
				size_t cnt=0;
				while(cnt< sizeof(buf)){
					n = read(newsd, buf+cnt, sizeof(buf)-cnt);
					if (n <= 0) {
						if (n < 0)
							perror("read from remote peer failed");
						else	
							fprintf(stderr, "Peer went away\n");
						away=1;
						break;
					}
					cnt+=n;
				}
				if(away) break;
				printf("(%s):",addrstr);
				//buf[sizeof(n)]='\0';
				//printf("(%s-%dbytes):%s|",addrstr,n,buf);
				memcpy(data.encrypted,buf,DATA_SIZE);
				cryp.src = data.encrypted;
				cryp.dst = data.decrypted;
				cryp.op = COP_DECRYPT;
				if (ioctl(cfd, CIOCCRYPT, &cryp)) {
					perror("ioctl(CIOCCRYPT)");
					return 1;
				}
//				memset(&buf,0,sizeof(buf));
				memcpy(buf,data.decrypted,DATA_SIZE);
				insist_write(stdout_fd, addr, len);
				if (insist_write(stdout_fd, buf, strlen(buf)) != strlen(buf)) {
					perror("write to remote peer failed");
					break;
				}
			}
			else if (FD_ISSET(stdin_fd,&read_from))
			{
				memset(&buf[0],0,DATA_SIZE);
				n = read(stdin_fd, buf, DATA_SIZE-1);
				if (n <= 0) {
					if (n < 0)
						perror("read from remote peer failed");
					else	
						fprintf(stderr, "Peer went away\n");
					break;
				}
//				int i;
				buf[n]='\0';
//				for(i=0;i<n;i++)
//					data.in[i]=buf[i];
				memcpy(data.in,buf,n+1);
				if (DBG) 
					printf("ME:%s",data.in);
				cryp.src = data.in;
				cryp.dst = data.encrypted;
				cryp.op = COP_ENCRYPT;
				if (ioctl(cfd, CIOCCRYPT, &cryp)) {
					perror("ioctl(CIOCCRYPT)");
					return 1;
				}
				memset(&buf[0],0,sizeof(buf));
//				for(i=0;i<DATA_SIZE;i++)
//					buf[i]=data.encrypted;
				memcpy(buf,data.encrypted,DATA_SIZE);
				if (DBG) {
					printf("ME/encrypted:");
					for(i=0;i<DATA_SIZE; i++)
						printf("%x", buf[i]);
					printf("\n");
				}
				if (insist_write(newsd, buf, DATA_SIZE) != DATA_SIZE) {
					perror("write to remote peer failed");
					break;
				}
				
			}
		}
		if (ioctl(cfd, CIOCFSESSION, &sess.ses)) {
			perror("ioctl(CIOCFSESSION)");
			return 1;
		}
		
		/* Make sure we don't leak open files */
		if (close(newsd) < 0)
			perror("close");
		if (close(cfd) < 0)
			perror("close");
		
	}

	/* This will never happen */
	return 1;
}

