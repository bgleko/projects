#include <stdio.h>
#include<stdlib.h>
#include<time.h>

FILE *f1,*f2;

typedef struct position_t
{
	int x; //suntetagmenes shmeiou monopatiou
	int y;
	int counter; //posa shmeia apo monopatia exoun auto to shmeio ws prohgoumeno
	struct position_t *prev;//deixnei sto prohgoumeno shmeio tou monopatiou
}
last_position;

typedef struct Node 
{
	last_position *route;//monopati pou odhgei se auth th katastash
	int x;
	int y;
	int g; /* g-value of current node – κόστος μετάβασης */
	int h; /* h-value of current node – υπολειπόμενη απόσταση*/
	struct Node *next; //epomenh katastash pros elegxo
} SearchGraphNode;

int metrhths3;

void printpath(int robot,int **path,int length)
{
	int i;
	if (robot==0) fprintf(f1,"Robot 1:\n");
	else fprintf(f1,"Robot 2:\n");
	for(i=1;i<=length;i++)
	{
		fprintf(f1,"%d %d\n",path[i][1],path[i][0]);
	}
}

int admissible (int x1, int y1,int x2, int y2)
{
	return abs(x2-x1)+abs(y2-y1);
}

int non_admissible (int x1, int y1,int x2, int y2)
{
	return (abs(x2-x1)+abs(y2-y1))*(abs(x2-x1)+abs(y2-y1));
}

int **findpath(last_position *route,int length,int *full_length)
{
	int **found=NULL;
	if (route==NULL)
	{
		found=(int**)malloc((length+1)*sizeof(int*));
		int i;
		for(i=0;i<=length;i++)
		{
			found[i]=(int*)malloc(2*sizeof(int));
		}
		*full_length=length;
		found[0][0]=length-1;
		found[0][1]=length-1;
		return found;
	}
	found=findpath(route->prev, length+1,full_length);
	found[*full_length-length-1][0]=route->x;
	found[*full_length-length-1][1]=route->y;
	return found;
}

void cleareverything(SearchGraphNode **searchtree)
{
	SearchGraphNode *todelete;
	last_position *deleteroute;
	while((*searchtree)!=NULL)
	{
		while((*searchtree)->route!=NULL)
		{
			(*searchtree)->route->counter--;
			if((*searchtree)->route->counter==0)
			{
				deleteroute=(*searchtree)->route;
				(*searchtree)->route=(*searchtree)->route->prev;
				free(deleteroute);
			}
			else break;
		}
		todelete=(*searchtree);
		*searchtree=(*searchtree)->next;
		free(todelete);
	}
}

void destroyarray(int N, int M, int ***array)
{
	int i,j;
	for(i=0;i<=N;i++)
	{
		for(j=0;j<=M;j++)
		{
			free(array[i][j]);
		}
		free(array[i]);
	}
	free(array);
}

int **checknextstate(int robot,SearchGraphNode *searchtree,int current_x,int current_y,int new_x,int new_y,int end_posx,int end_posy,int ***array,char **map,int **other_route,int other_route_length,int *check)
{
	if(map[new_x][new_y]=='X'){return NULL;}
	else printf("Robot %d checked %d %d %d %d going to %d %d\n",robot+1,current_y,current_x,new_y,new_x,end_posy,end_posx);
	if (other_route_length>=searchtree->g+1)//mporei na thelhsei >=
	{
		if((other_route[(searchtree->g+1)][0]==new_x && other_route[(searchtree->g+1)][1]==new_y) || (other_route[(searchtree->g)][0]==new_x && other_route[(searchtree->g)][1]==new_y && other_route[(searchtree->g+1)][0]==current_x && other_route[(searchtree->g+1)][1]==current_y)) 
		{
			*check=1;
			if(!robot)
			{
				fprintf(f2,"Robot 1 considering new position <%d,%d> at step %d\n",new_y,new_x,searchtree->g+1);
				fprintf(f2,"Robot 2 considering new position <%d,%d> at step %d\n",other_route[(searchtree->g+1)][1],other_route[(searchtree->g+1)][0],searchtree->g+1);
				fprintf(f2,"Conflict. Robot 1 reconsidering position\n");
			}
			else
			{
				fprintf(f2,"Robot 2 considering new position <%d,%d> at step %d\n",new_y,new_x,searchtree->g+1);
				fprintf(f2,"Robot 1 considering new position <%d,%d> at step %d\n",other_route[(searchtree->g+1)][1],other_route[(searchtree->g+1)][0],searchtree->g+1);
				fprintf(f2,"Conflict. Robot 2 reconsidering position\n");
			}
			return NULL;
		}
	}
	SearchGraphNode *temp=NULL,*temp2,*todelete;
	temp2=searchtree;
	last_position *temp_route;
	int current_cost;
	if (current_x==new_x && current_y==new_y)current_cost=admissible(new_x,new_y,end_posx,end_posy)+2; //upologizoume kostos neas theshs
	else current_cost=admissible(new_x,new_y,end_posx,end_posy);
	if (array[new_x][new_y][0]==0 || array[new_x][new_y][1]>temp2->g+1 || (current_x==new_x && current_y==new_y)) //an prokeitai gia nea thesh h palia thesh me veltiwmeno sunoliko kostos exoume apodekth katastash
	{
		metrhths3++;
		int old_cost=temp2->g;
		if (current_x!=new_x || current_y!=new_y) array[new_x][new_y][1]=temp2->g+1; //orise to kostos neas katastashs
		temp=(SearchGraphNode *)malloc(sizeof(SearchGraphNode)); //dhmiourghse nea katastash
		temp->x=new_x;//apothikeuse suntetagmenes
		temp->y=new_y;
		temp->next=NULL;
		temp_route=(last_position*)malloc(sizeof(last_position));//dhmiourghse neo kombo gia to monopati
		temp_route->x=current_x;
		temp_route->y=current_y;
		temp_route->counter=1;
		temp_route->prev=temp2->route;//krata to monopati mexri thn katastash
		temp->route=temp_route;
		if(temp_route->prev!=NULL) temp_route->prev->counter++;//aukshse kata ena tis fores pou summetexoun se monopatia oi suntetagmenes tou proteleutaiou bhmatos
		if(array[new_x][new_y][0]==1 && (current_x!=new_x || current_y!=new_y)) //an to tetragwno eixe hdh xrhsimopoihthei apo monopati, ara brhkame kalutero monopati gia to tetragwno
		{
			temp2=searchtree;
			while(temp2->next!=NULL) //psakse mexri na vreis monopati(to opoio tha nai xeirotero apo to twrino) pou odhgei sto idio tetragwno
			{
				if(temp2->next->x==new_x && temp2->next->y==new_y)
				{
					while(temp2->next->route!=NULL)
					{
						temp2->next->route->counter--;//meiwse kata ena tis fores pou summetexei to tetragwno se monopatia
						if (temp2->next->route->counter==0)//an den summeteixe se allo monopati tote diegrapse to teleiws
						{
							temp_route=temp2->next->route;
							temp2->next->route=temp2->next->route->prev;
							free(temp_route);
						}
						else break;
					}
					todelete=temp2->next;//molis diagrapseis ta monopatia sbhse kai thn palia xeiroterh katastash
					temp2->next=temp2->next->next;
					free(todelete);
				}
				else temp2=temp2->next;//an den thn exeis vrei akoma psakse sthn epomenh
			}
		}
		else
		{
			array[new_x][new_y][0]=1;//an den eixe vrethei hdh se auto to tetragwno orise oti exei vrethei
		}
		temp2=searchtree;
		if (current_x==new_x && current_y==new_y) temp->g=old_cost+1;
		else temp->g=old_cost+1;//topotheteise to neo kostos sthn katastash
		temp->h=current_cost;//topotheteise to upologizomeno upoloipo kostos sthn katastash
		while(temp2->next!=NULL)
		{
			if((temp->h+temp->g)<(temp2->next->h+temp2->next->g))//psakse apo thn arxh ths listas
			{													//mexri na vreis th taksinomhmenh thesh ths katastashs
				temp->next=temp2->next->next;//an th vrhkes prosthese to
				temp2->next=temp;
				break;
			}
			else temp2=temp2->next;//alliws psakse sto epomeno
		}
		if (temp2->next==NULL)
		{
			temp->next=NULL;//an den vrhkes tote prepei na paei sto telos
			temp2->next=temp;
		}
		if (new_x==end_posx && new_y==end_posy)
		{
			int a=0;
			int **found=findpath(temp->route,1,&a);
			found[a-1][0]=new_x;
			found[a-1][1]=new_y;
			found[0][0]=a-1;
			found[0][1]=a-1;
			cleareverything(&searchtree);
			return found;
		}
	}
	return NULL;
}

void newarray(int N,int M,int ***array)
{
	int i,j;
	for(i=0;i<=N;i++)
	{
		for(j=0;j<=M;j++)
		{
			array[i][j][0]=0;
			array[i][j][1]=0;
			//array[N+1][M+1][2]=0;
		}
	}
	return;
}

int **astaralgorithm(int robot,int N, int M, int start_posx,int start_posy,int end_posx,int end_posy,char **map,int **other_route,int other_route_length)
{
	int ***array;
	SearchGraphNode *searchtree;
	int i,j;
	array=(int ***)malloc((N+1)*sizeof(int**));
	int check;
	for(i=0;i<=N;i++)
	{
		array[i]=(int**)malloc((M+1)*sizeof(int*));
		for(j=0;j<=M;j++)
		{
			array[i][j]=(int*)malloc(2*sizeof(int));
		}
	}
	newarray(N,M,array);
	searchtree=(SearchGraphNode *)malloc(sizeof(SearchGraphNode));
	searchtree->route=NULL;
	searchtree->x=start_posx;
	searchtree->y=start_posy;
	searchtree->next=NULL;
	array[start_posx][start_posy][0]=1;
	int g_value=0;
	int h_value=admissible(start_posx,start_posy,end_posx,end_posy);
	searchtree->g=g_value;
	searchtree->h=h_value;
	SearchGraphNode *temp;
	int current_x;
	int current_y;
	int **found;
	last_position *temp_route;
	while (searchtree!=NULL)
	{
		check=0;
		current_x=searchtree->x;
		current_y=searchtree->y;
		//array[current_x][current_y][1]++;
		if (current_x-1>0)
		{
			found=checknextstate(robot,searchtree,current_x,current_y,current_x-1,current_y,end_posx,end_posy,array,map,other_route,other_route_length,&check);//nea katastash me kinhsh panw
			if(found!=NULL) 
			{
				destroyarray(N,M,array);
				return found;
			}
		}
		if (current_x+1<=N)
		{
			found=checknextstate(robot,searchtree,current_x,current_y,current_x+1,current_y,end_posx,end_posy,array,map,other_route,other_route_length,&check);//nea katastash me kinhsh katw
			if(found!=NULL) 
			{
				destroyarray(N,M,array);
				return found;
			}
		}
		if (current_y-1>0)
		{
			found=checknextstate(robot,searchtree,current_x,current_y,current_x,current_y-1,end_posx,end_posy,array,map,other_route,other_route_length,&check);//nea katastash me kinhsh aristera
			if(found!=NULL) 
			{
				destroyarray(N,M,array);
				return found;
			}
		}
		if (current_y+1<=M)
		{
			found=checknextstate(robot,searchtree,current_x,current_y,current_x,current_y+1,end_posx,end_posy,array,map,other_route,other_route_length,&check);//nea katastash me kinhsh deksia
			if(found!=NULL) 
			{
				destroyarray(N,M,array);
				return found;
			}
		}
		if (check)
		{
			found=checknextstate(robot,searchtree,current_x,current_y,current_x,current_y,end_posx,end_posy,array,map,other_route,other_route_length,&check);//nea katastash paramenei akinhto
			if(found!=NULL) 
			{
				destroyarray(N,M,array);
				return found;
			}
		}
		temp=searchtree;
		while(searchtree->route!=NULL)
		{
			temp_route=searchtree->route;
			searchtree->route->counter--;
			if (searchtree->route->counter==0)
			{
				searchtree->route=searchtree->route->prev;
				free(temp_route);
			}
			else
			{
				break;
			}
		}
		searchtree=searchtree->next;
		free(temp);
	}
	for(i=0;i<=N;i++)
	{
		for(j=0;j<=M;j++)
		{
			free(array[i][j]);
		}
		free(array[i]);
	}
	free(array);
	return NULL;
}

int main()
{
	clock_t start = clock();
	metrhths3=0;
	FILE *f;
	f=fopen("input.txt","r");
	f1=fopen("output1.txt","w");
	f2=fopen("output2.txt","w");
	int i,j;
	int N,M;
	fscanf(f,"%d %d", &M, &N);//diabazoume megethos xwrou
	int x1,y1;
	int x2,y2;
	fscanf(f,"%d %d", &y1, &x1);//diabazoume arxikes suntetagmenes rompot
	fscanf(f,"%d %d", &y2, &x2);
	int end_posx,end_posy;
	fscanf(f,"%d %d", &end_posy,&end_posx);
	int count;
	fscanf(f,"%d",&count);//diabazoume to plhthos twn epithumhtwn stoxwn
	int **places;
	places=(int**)malloc((count+2)*sizeof(int*));
	for(i=0;i<=count+1;i++)
	{
		places[i]=(int*)malloc(2*sizeof(int));
	}
	for(i=1;i<=count;i++)
	{
		fscanf(f,"%d %d", &places[i][1], &places[i][0]);//diavase tous endiamesous stoxous
	}
	places[count+1][0]=end_posx;
	places[count+1][1]=end_posy;
	char **map;
	map=(char**)malloc((N+1)*sizeof(char*));
	for(i=0;i<=N;i++)
	{
		map[i]=(char*)malloc((M+1)*sizeof(char));
	}
	fscanf(f,"%c",&j);
	for(i=1;i<=N;i++)
	{
		for(j=1;j<=M;j++)
		{
			fscanf(f,"%c",&map[i][j]);//diabase ton pinaka
		}
		fscanf(f,"%c",&j);
	}
	fclose(f);
	f=fopen("output.txt","w");
	fprintf(f,"%d %d\n", M, N);
	fprintf(f,"%d %d\n", y1, x1);//ektupwnoume arxikes suntetagmenes rompot
	fprintf(f,"%d %d\n", y2, x2);
	fprintf(f,"%d %d\n", end_posy,end_posx);
	fprintf(f,"%d\n",count);//ektupwnoume to plhthos twn epithumhtwn stoxwn
	for(i=1;i<=count;i++)
	{
		fprintf(f,"%d %d\n", places[i][1], places[i][0]);//ektupwse tous endiamesous stoxous
	}
	for(i=1;i<=N;i++)
	{
		for(j=1;j<=M;j++)
		{
			fprintf(f,"%c",map[i][j]);//ektupwse ton pinaka
		}
		fprintf(f,"\n");
	}
	fclose(f);
	fprintf(f1,"Robot 1:\n");
	fprintf(f1,"%d %d\n", y1, x1);
	fprintf(f1,"Robot 2:\n");
	fprintf(f1,"%d %d\n", y2, x2);
	int **one_route,**two_route,one_route_length=0,two_route_length=0,first=0;
	two_route=(int**)malloc(sizeof(int*));
	two_route[0]=(int*)malloc(2*sizeof(int));
	two_route[0][0]=0;
	two_route[0][1]=0;
	one_route=astaralgorithm(0,N, M, x1,y1,places[1][0],places[1][1],map,two_route,two_route_length);//bres th diadromh tou prwtou rompot
	if(one_route==NULL)
	{
		printf("Some goals were unreachable\n");
		exit(0);
	}
	one_route_length=one_route[0][0];
	printpath(0,one_route,one_route_length);
	two_route=astaralgorithm(1,N, M, x2,y2,places[1][0],places[1][1],map,one_route,one_route_length);//bres th diadromh tou deuterou rompot
	if(two_route==NULL)
	{
		printf("Some goals were unreachable\n");
		exit(0);
	}
	two_route_length=two_route[0][0];
	printpath(1,two_route,two_route_length);
	if(two_route_length<one_route_length)
	{
		first=1;
		for(i=two_route_length+1;i<=one_route_length;i++)//an to prwto monopati teleiwnei meta to deutero
		{												//tote tha vrethei neo monopati gia to deutero rompot prwta
			one_route[i-two_route_length][0]=one_route[i][0];//metakinhse ta bhmata pou den exoun ginei sthn arxh
			one_route[i-two_route_length][1]=one_route[i][1];
		}
		for (i=one_route_length-two_route_length+1;i<=one_route_length;i++)
		{
			free(one_route[i]);//kai svhse ta upoloipa
		}
		one_route_length=one_route_length-two_route_length;
		for(i=0;i<=two_route_length;i++)
		{
			free(two_route[i]);//sth diadromh pou teleiwnei prwta sbhse ta panta
		}
		free(two_route);
		two_route_length=0;
	}
	else
	{
		first=0;
		for(i=one_route_length+1;i<=two_route_length;i++)//an to prwto monopati teleiwnei prin to deutero
		{												//tote tha vrethei neo monopati gia to deutero rompot prwta
			two_route[i-one_route_length][0]=two_route[i][0];//metakinhse ta bhmata pou den exoun ginei sthn arxh
			two_route[i-one_route_length][1]=two_route[i][1];
		}
		for (i=two_route_length-one_route_length+1;i<=two_route_length;i++)
		{
			free(two_route[i]);//kai svhse ta upoloipa
		}
		two_route_length=two_route_length-one_route_length;
		for(i=0;i<=one_route_length;i++)
		{
			free(one_route[i]);//sth diadromh pou teleiwnei prwta sbhse ta panta
		}
		free(one_route);
		one_route_length=0;
	}
	int k,l;
	k=2;
	l=2;
	while(k<=count+1 && l<=count+1)
	{
		if (first==0)//an molis teleiwse th diadromh tou to prwto rompot
		{
			one_route=astaralgorithm(0,N, M,places[k-1][0],places[k-1][1],places[k][0],places[k][1],map,two_route,two_route_length);//bres th diadromh tou prwtou rompot
			if(one_route==NULL)
			{
				printf("Some goals were unreachable\n");
				exit(0);
			}
			one_route_length=one_route[0][0];
			printpath(0,one_route,one_route_length);
			k++;
		}
		else//an molis teleiwse th diadromh tou to deytero rompot
		{
			two_route=astaralgorithm(1,N, M,places[l-1][0],places[l-1][1],places[l][0],places[l][1],map,one_route,one_route_length);//bres th diadromh tou prwtou rompot
			if(two_route==NULL)
			{
				printf("Some goals were unreachable\n");
				exit(0);
			}
			two_route_length=two_route[0][0];
			printpath(1,two_route,two_route_length);
			l++;
		}
		if(two_route_length<one_route_length)
		{
			first=1;
			for(i=two_route_length+1;i<=one_route_length;i++)//an to prwto monopati teleiwnei meta to deutero
			{												//tote tha vrethei neo monopati gia to deutero rompot prwta
				one_route[i-two_route_length][0]=one_route[i][0];//metakinhse ta bhmata pou den exoun ginei sthn arxh
				one_route[i-two_route_length][1]=one_route[i][1];
			}
			for (i=one_route_length-two_route_length+1;i<=one_route_length;i++)
			{
				free(one_route[i]);//kai svhse ta upoloipa
			}
			one_route_length=one_route_length-two_route_length;
			for(i=0;i<=two_route_length;i++)
			{
				free(two_route[i]);//sth diadromh pou teleiwnei prwta sbhse ta panta
			}
			free(two_route);
			two_route_length=0;
		}
		else
		{
			first=0;
			for(i=one_route_length+1;i<=two_route_length;i++)//an to prwto monopati teleiwnei prin to deutero
			{												//tote tha vrethei neo monopati gia to deutero rompot prwta
				two_route[i-one_route_length][0]=two_route[i][0];//metakinhse ta bhmata pou den exoun ginei sthn arxh
				two_route[i-one_route_length][1]=two_route[i][1];
			}
			for (i=two_route_length-one_route_length+1;i<=two_route_length;i++)
			{	
				free(two_route[i]);//kai svhse ta upoloipa
			}
			two_route_length=two_route_length-one_route_length;
			for(i=0;i<=one_route_length;i++)
			{
				free(one_route[i]);//sth diadromh pou teleiwnei prwta sbhse ta panta
			}	
			free(one_route);
		}
	}
	char oldmap;
	if(k<=count+1 && l>count+1)//an to deutero rompot exei apofasisei oles tis diadromes tou
	{
		for (i=k;i<=count+1;i++)
		{
			if (i==count+1)
			{
				oldmap=map[end_posx][end_posy];
				map[end_posx][end_posy]='X';
				if (end_posx-1>0 && map[end_posx-1][end_posy]!='X') one_route=astaralgorithm(0,N, M,places[count][0],places[count][1],places[count+1][0]-1,places[count+1][1],map,two_route,two_route_length);//bres th diadromh tou deuterou rompot
				else if (end_posx+1<=N && map[end_posx+1][end_posy]!='X') one_route=astaralgorithm(0,N, M,places[count][0],places[count][1],places[count+1][0]+1,places[count+1][1],map,two_route,two_route_length);//bres th diadromh tou deuterou rompot
				else if (end_posy-1>0 && map[end_posx][end_posy-1]!='X') one_route=astaralgorithm(0,N, M,places[count][0],places[count][1],places[count+1][0],places[count+1][1]-1,map,two_route,two_route_length);//bres th diadromh tou deuterou rompot
				else if (end_posy+1<=M && map[end_posx][end_posy+1]!='X') one_route=astaralgorithm(0,N, M,places[count][0],places[count][1],places[count+1][0],places[count+1][1]+1,map,two_route,two_route_length);//bres th diadromh tou deuterou rompot
				else
				{
					printf("Goal Unreachable\n");
					exit(0);
				}
				if(one_route==NULL)
				{
					printf("Some goals were unreachable\n");
					exit(0);
				}
				map[end_posx][end_posy]=oldmap;
				one_route_length=one_route[0][0];
				printpath(0,one_route,one_route_length);
			}
			else
			{
			
				one_route=astaralgorithm(0,N, M,places[i-1][0],places[i-1][1],places[i][0],places[i][1],map,two_route,two_route_length);//bres th diadromh tou prwtou rompot
				if(one_route==NULL)
				{
					printf("Some goals were unreachable\n");
					exit(0);
				}		
				one_route_length=one_route[0][0];
				printpath(0,one_route,one_route_length);
			}
			if(two_route_length<one_route_length && two_route_length!=0)
			{
				for(j=two_route_length+1;j<=one_route_length;j++)//an to deutero rompot stamathsei prin ftasei ston epomeno stoxo to prwto
				{	
					one_route[j-two_route_length][0]=one_route[j][0];//svhnoume osa kommatia tou monopatiou exoun hdh ginei
					one_route[j-two_route_length][1]=one_route[j][1];
				}
				for (j=0;j<=one_route_length;j++)
				{
					free(one_route[j]);//kai svhnoume ta panta
				}
				free(one_route);//gia na vroume kai tis upoloipees diadromes
				one_route_length=0;
				for(j=0;j<=two_route_length;j++)
				{
					free(two_route[j]);
				}
				free(two_route);
				two_route_length=0;
			}
			else if (one_route_length<=two_route_length && two_route_length!=0)
			{
				for(j=one_route_length+1;j<=two_route_length;j++)//an to prwto rompot stamathsei prin ftasei ston epomeno stoxo to deutero
				{
					two_route[j-one_route_length][0]=two_route[j][0];//svhnoume osa kommatia tou monopatiou exoun hdh ginei
					two_route[j-one_route_length][1]=two_route[j][1];
				}
				for (j=two_route_length-one_route_length+1;j<=two_route_length;j++)
				{	
					free(two_route[j]);//kai kratame osa kommatia ths teleutaias kinhshs menoun na ginoun
				}
				two_route_length=two_route_length-one_route_length;
				for(j=0;j<=one_route_length;j++)
				{
					free(one_route[j]);
				}	
			}
			else
			{
				for(j=0;j<=one_route_length;j++)
				{
					free(one_route[j]);//an kineitai mono to ena rompot meta thn ektelesh ths diadromhs svhse to monopati
				}
				free(one_route);
			}
		}
	}
	else//an to prwto rompot exei apofasisei oles tis diadromes tou
	{
		for (i=l;i<=count+1;i++)
		{
			if (i==count+1)
			{
				if (end_posx-1>0 && map[end_posx-1][end_posy]!='X') two_route=astaralgorithm(1,N, M,places[count][0],places[count][1],places[count+1][0]-1,places[count+1][1],map,one_route,one_route_length);//bres th diadromh tou deuterou rompot
				else if (end_posx+1<=N && map[end_posx+1][end_posy]!='X') two_route=astaralgorithm(1,N, M,places[count][0],places[count][1],places[count+1][0]+1,places[count+1][1],map,one_route,one_route_length);//bres th diadromh tou deuterou rompot
				else if (end_posy-1>0 && map[end_posx][end_posy-1]!='X') two_route=astaralgorithm(1,N, M,places[count][0],places[count][1],places[count+1][0],places[count+1][1]-1,map,one_route,one_route_length);//bres th diadromh tou deuterou rompot
				else if (end_posy+1<=M && map[end_posx][end_posy+1]!='X') two_route=astaralgorithm(1,N, M,places[count][0],places[count][1],places[count+1][0],places[count+1][1]+1,map,one_route,one_route_length);//bres th diadromh tou deuterou rompot
				else
				{
					printf("Goal Unreachable\n");
					exit(0);
				}
				if(two_route==NULL)
				{
					printf("Some goals were unreachable\n");
					exit(0);
				}
				two_route_length=two_route[0][0];
				printpath(1,two_route,two_route_length);
			}
			else
			{
				two_route=astaralgorithm(1,N, M,places[i-1][0],places[i-1][1],places[i][0],places[i][1],map,one_route,one_route_length);//bres th diadromh tou deuterou rompot
				if(two_route==NULL)
				{
					printf("Some goals were unreachable\n");
					exit(0);
				}
				two_route_length=two_route[0][0];
				printpath(1,two_route,two_route_length);
			}
			if(one_route_length<two_route_length && one_route_length!=0)
			{
				for(j=one_route_length+1;j<=two_route_length;j++)
				{	
					two_route[j-one_route_length][0]=two_route[j][0];//svhnoume osa kommatia tou monopatiou exoun hdh ginei
					two_route[j-one_route_length][1]=two_route[j][1];
				}
				for (j=0;j<=two_route_length;j++)
				{
					free(two_route[j]);//kai sbhnoume ta panta
				}
				free(two_route);
				two_route_length=0;
				for(j=0;i<=one_route_length;j++)
				{
					free(one_route[j]);
				}
				free(one_route);
				one_route_length=0;
			}
			else if (two_route_length<=one_route_length && one_route_length!=0)//bres th diadromh tou prwtou rompot
			{
				for(j=two_route_length+1;j<=one_route_length;j++)
				{
					one_route[j-two_route_length][0]=one_route[j][0];//svhnoume osa kommatia tou monopatiou exoun hdh ginei
					one_route[j-two_route_length][1]=one_route[j][1];//ta upoloipa phgaine ta sthn arxh
				}
				for (j=one_route_length-two_route_length+1;j<=one_route_length;j++)
				{	
					free(one_route[j]);
				}
				one_route_length=one_route_length-two_route_length;//sunxexise se epomeno monopati me eisodo thn upoloiph diadromh pou paramenei
				for(j=0;j<=two_route_length;j++)
				{
					free(two_route[j]);
				}	
			}
			else
			{
				for(j=0;j<=two_route_length;j++)
				{
					free(two_route[j]);//an kineitai mono to ena rompot meta thn ektelesh ths diadromhs svhse to monopati
				}
				free(two_route);
			}
		}
	}	
	for(i=0;i<=count;i++)
	{
		free(places[i]);
	}
	for(i=0;i<=N;i++)
	{
		free(map[i]);
	}
	free(map);
	free(places);
	printf("Did %d checks\n",metrhths3);
	
	clock_t end = clock();
	float seconds = (float)(end - start) / CLOCKS_PER_SEC;
	printf("Duration: %lf sec\n",seconds);
	close(f2);
	close(f1);
	return 0;
}